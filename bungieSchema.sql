CREATE DATABASE  IF NOT EXISTS `BungieData` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `BungieData`;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
--
-- Database: `BungieData`
--

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityBundleDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityBundleDefinition` (
  `bundleHash` bigint(20) NOT NULL,
  `activityName` varchar(256) NOT NULL,
  `activityDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `releaseIcon` varchar(256) NOT NULL,
  `releaseTime` int(11) NOT NULL,
  `destinationHash` bigint(20) NOT NULL,
  `placeHash` bigint(20) NOT NULL,
  `activityTypeHash` bigint(20) NOT NULL,
  KEY `bundleHash` (`bundleHash`,`activityName`,`activityDescription`,`destinationHash`,`placeHash`,`activityTypeHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityBundleDefinition_activityHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityBundleDefinition_activityHashes` (
  `bundleHash` bigint(20) NOT NULL,
  `activityHashes` bigint(20) NOT NULL,
  KEY `bundleHash` (`bundleHash`,`activityHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityDefinition` (
  `activityHash` bigint(20) NOT NULL,
  `activityName` varchar(256) NOT NULL,
  `activityDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `releaseIcon` varchar(256) NOT NULL,
  `releaseTime` varchar(256) NOT NULL,
  `activityLevel` int(11) NOT NULL,
  `completionFlagHash` bigint(20) NOT NULL,
  `activityPower` varchar(256) NOT NULL,
  `minParty` int(11) NOT NULL,
  `maxParty` int(11) NOT NULL,
  `maxPlayers` int(11) NOT NULL,
  `destinationHash` bigint(20) NOT NULL,
  `placeHash` bigint(20) NOT NULL,
  `activityTypeHash` bigint(20) NOT NULL,
  `tier` varchar(256) NOT NULL,
  `pgcrImage` varchar(256) NOT NULL,
  KEY `activityHash` (`activityHash`,`activityName`,`activityDescription`,`activityLevel`,`completionFlagHash`,`minParty`,`maxParty`,`maxPlayers`,`destinationHash`,`placeHash`,`activityTypeHash`,`tier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityDefinition_rewards`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityDefinition_rewards` (
  `activityHash` bigint(20) NOT NULL,
  KEY `activityHash` (`activityHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityDefinition_rewards_rewardItems`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityDefinition_rewards_rewardItems` (
  `activityHash` bigint(20) NOT NULL,
  `itemHash` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  KEY `activityHash` (`activityHash`,`itemHash`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityDefinition_skulls`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityDefinition_skulls` (
  `activityHash` bigint(20) NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  PRIMARY KEY (`activityHash`,`displayName`),
  KEY `activityHash` (`activityHash`,`displayName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyActivityTypeDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyActivityTypeDefinition` (
  `activityTypeHash` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `activityTypeName` varchar(256) NOT NULL,
  `activityTypeDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `activeBackgroundVirtualPath` varchar(256) NOT NULL,
  `completedBackgroundVirtualPath` varchar(256) NOT NULL,
  `hiddenOverrideVirtualPath` varchar(256) NOT NULL,
  `tooltipBackgroundVirtualPath` varchar(256) NOT NULL,
  `enlargedActiveBackgroundVirtualPath` varchar(256) NOT NULL,
  `enlargedCompletedBackgroundVirtualPath` varchar(256) NOT NULL,
  `enlargedHiddenOverrideVirtualPath` varchar(256) NOT NULL,
  `enlargedTooltipBackgroundVirtualPath` varchar(256) NOT NULL,
  `orderDGS` varchar(256) NOT NULL,
  `statGroup` varchar(256) NOT NULL,
  `friendlyUrlId` varchar(256) NOT NULL,
  KEY `activityTypeHash` (`activityTypeHash`,`identifier`,`activityTypeName`,`statGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyClassDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyClassDefinition` (
  `classHash` bigint(20) NOT NULL,
  `classType` int(11) NOT NULL,
  `className` varchar(256) NOT NULL,
  `classNameMale` varchar(256) NOT NULL,
  `classNameFemale` varchar(256) NOT NULL,
  `classIdentifier` varchar(256) NOT NULL,
  `mentorVendorIdentifier` varchar(256) NOT NULL,
  PRIMARY KEY (`classHash`),
  KEY `classHash` (`classHash`,`classType`,`className`,`classNameMale`,`classNameFemale`,`classIdentifier`,`mentorVendorIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDestinationDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyDestinationDefinition` (
  `destinationDescription` varchar(256) NOT NULL,
  `destinationHash` bigint(20) NOT NULL,
  `destinationIdentifier` varchar(256) NOT NULL,
  `destinationName` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `locationIdentifier` varchar(256) NOT NULL,
  `placeHash` bigint(20) NOT NULL,
  KEY `destinationDescription` (`destinationDescription`,`destinationHash`,`destinationIdentifier`,`destinationName`,`locationIdentifier`,`placeHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition` (
  `bookHash` bigint(20) NOT NULL,
  `bookName` varchar(256) NOT NULL,
  `bookDescription` varchar(256) NOT NULL,
  `bookIdentifier` varchar(256) NOT NULL,
  `visible` int(11) NOT NULL,
  `isOverview` int(11) NOT NULL,
  `urlFriendlyName` varchar(256) NOT NULL,
  `destinationHash` bigint(20) NOT NULL,
  `mapImage` varchar(256) NOT NULL,
  KEY `bookHash` (`bookHash`,`bookName`,`bookDescription`,`bookIdentifier`,`visible`,`isOverview`,`destinationHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_isVisibleExpression`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_isVisibleExpression` (
  `bookHash` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_isVisibleExpression_steps`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_isVisibleExpression_steps` (
  `bookHash` bigint(20) NOT NULL,
  `stepOperator` bigint(20) NOT NULL,
  `flagHash` bigint(20) NOT NULL,
  `valueHash` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`stepOperator`,`flagHash`,`valueHash`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_nodes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_nodes` (
  `bookHash` bigint(20) NOT NULL,
  `nodeDefinitionHash` bigint(20) NOT NULL,
  `styleHash` bigint(20) NOT NULL,
  `positionX` bigint(20) NOT NULL,
  `positionY` bigint(20) NOT NULL,
  `positionZ` bigint(20) NOT NULL,
  `uiModifier` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`nodeDefinitionHash`,`styleHash`,`positionX`,`positionY`,`positionZ`,`uiModifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_nodes_activityBundleHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_nodes_activityBundleHashes` (
  `bookHash` bigint(20) NOT NULL,
  `nodeDefinitionHash` bigint(20) NOT NULL,
  `activityBundleHashes` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`nodeDefinitionHash`,`activityBundleHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_nodes_states`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_nodes_states` (
  `bookHash` bigint(20) NOT NULL,
  `nodeDefinitionHash` bigint(20) NOT NULL,
  `state` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`nodeDefinitionHash`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_nodes_states_expression`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_nodes_states_expression` (
  `bookHash` bigint(20) NOT NULL,
  `nodeDefinitionHash` bigint(20) NOT NULL,
  `state` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`nodeDefinitionHash`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_nodes_states_expression_steps`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_nodes_states_expression_steps` (
  `bookHash` bigint(20) NOT NULL,
  `nodeDefinitionHash` bigint(20) NOT NULL,
  `state` bigint(20) NOT NULL,
  `stepOperator` bigint(20) NOT NULL,
  `flagHash` bigint(20) NOT NULL,
  `valueHash` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`nodeDefinitionHash`,`state`,`stepOperator`,`flagHash`,`valueHash`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_notificationNodes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_notificationNodes` (
  `bookHash` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_notificationNodes_nodes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_notificationNodes_nodes` (
  `bookHash` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `transitionNodeIdentifier` varchar(256) NOT NULL,
  `positionX` bigint(20) NOT NULL,
  `positionY` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `backgroundImagePath` varchar(256) NOT NULL,
  `showCount` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`identifier`,`transitionNodeIdentifier`,`positionX`,`positionY`,`width`,`height`,`backgroundImagePath`,`showCount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_notificationNodes_nodes_aBHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_notificationNodes_nodes_aBHashes` (
  `bookHash` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `transitionNodeIdentifier` varchar(256) NOT NULL,
  `activityBundleHashes` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`identifier`,`transitionNodeIdentifier`,`activityBundleHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_notificationNodes_nodes_vHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_notificationNodes_nodes_vHashes` (
  `bookHash` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `transitionNodeIdentifier` varchar(256) NOT NULL,
  `vendorHashes` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`identifier`,`transitionNodeIdentifier`,`vendorHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_tileMap`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_tileMap` (
  `bookHash` bigint(20) NOT NULL,
  `numColumns` bigint(20) NOT NULL,
  `numRows` bigint(20) NOT NULL,
  `tileWidth` bigint(20) NOT NULL,
  `tileHeight` bigint(20) NOT NULL,
  KEY `bookHash` (`bookHash`,`numColumns`,`numRows`,`tileWidth`,`tileHeight`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_tileMap_tileImages`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_tileMap_tileImages` (
  `bookHash` bigint(20) NOT NULL,
  `tileImages` varchar(256) NOT NULL,
  KEY `bookHash` (`bookHash`,`tileImages`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyDirectorBookDefinition_transitionNodes`
--

CREATE TABLE IF NOT EXISTS `DestinyDirectorBookDefinition_transitionNodes` (
  `bookHash` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `positionX` bigint(20) NOT NULL,
  `positionY` bigint(20) NOT NULL,
  `transitionBookHash` bigint(20) NOT NULL,
  `transitionType` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `parallaxIndex` bigint(20) NOT NULL,
  `imagePath` varchar(256) NOT NULL,
  `alphaImagePath` varchar(256) NOT NULL,
  `destinationBackgroundImagePath` varchar(256) NOT NULL,
  `destinationDetailImagePath` varchar(256) NOT NULL,
  `label` varchar(256) NOT NULL,
  KEY `bookHash` (`bookHash`,`identifier`,`positionX`,`positionY`,`transitionBookHash`,`transitionType`,`width`,`height`,`parallaxIndex`,`imagePath`,`alphaImagePath`,`destinationBackgroundImagePath`,`destinationDetailImagePath`,`label`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyFactionDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyFactionDefinition` (
  `factionHash` bigint(20) NOT NULL,
  `factionName` varchar(256) NOT NULL,
  `factionDescription` varchar(256) NOT NULL,
  `factionIcon` varchar(256) NOT NULL,
  `progressionHash` bigint(20) NOT NULL,
  KEY `factionHash` (`factionHash`,`factionName`,`factionDescription`,`factionIcon`,`progressionHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGenderDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyGenderDefinition` (
  `genderHash` bigint(20) NOT NULL,
  `genderType` bigint(20) NOT NULL,
  `genderName` varchar(256) NOT NULL,
  `genderDescription` varchar(256) NOT NULL,
  KEY `genderHash` (`genderHash`,`genderType`,`genderName`,`genderDescription`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition` (
  `cardId` bigint(20) NOT NULL,
  `cardName` varchar(256) NOT NULL,
  `cardIntro` varchar(256) NOT NULL,
  `cardDescription` varchar(4000) NOT NULL,
  `unlockHowToText` varchar(256) NOT NULL,
  `cardLabel` varchar(256) NOT NULL,
  `rarity` bigint(20) NOT NULL,
  `unlockFlagHash` bigint(20) NOT NULL,
  `points` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`,`cardName`,`cardIntro`,`cardDescription`(767),`unlockHowToText`,`cardLabel`,`rarity`,`unlockFlagHash`,`points`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes` (
  `cardId` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_image`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_image` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_image_rect` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_image_sheetSize` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_smallImage` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_smallImage_rect` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_highRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_highRes_smallImage_sheetSize` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  KEY `cardId` (`cardId`,`sheetPath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes` (
  `cardId` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_image`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_image` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_image_rect` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_image_sheetSize` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_smallImage` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_smallImage_rect` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyGrimoireCardDefinition_normRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DestinyGrimoireCardDefinition_normRes_smallImage_sheetSize` (
  `cardId` bigint(20) NOT NULL,
  `sheetPath` varchar(256) NOT NULL,
  `x` bigint(20) NOT NULL,
  `y` bigint(20) NOT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyHistoricalStatsDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyHistoricalStatsDefinition` (
  `statId` varchar(256) NOT NULL,
  `groupDGS` bigint(20) NOT NULL,
  `category` bigint(20) NOT NULL,
  `statName` varchar(256) NOT NULL,
  `statDescription` varchar(256) NOT NULL,
  `unitType` bigint(20) NOT NULL,
  `iconImage` varchar(256) NOT NULL,
  `unitLabel` varchar(256) NOT NULL,
  `weight` bigint(20) NOT NULL,
  KEY `statId` (`statId`,`groupDGS`,`category`,`statName`,`unitType`,`unitLabel`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyHistoricalStatsDefinition_modes`
--

CREATE TABLE IF NOT EXISTS `DestinyHistoricalStatsDefinition_modes` (
  `statId` varchar(256) NOT NULL,
  `modes` bigint(20) NOT NULL,
  KEY `statId` (`statId`,`modes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyHistoricalStatsDefinition_periodTypes`
--

CREATE TABLE IF NOT EXISTS `DestinyHistoricalStatsDefinition_periodTypes` (
  `statId` varchar(256) NOT NULL,
  `periodTypes` bigint(20) NOT NULL,
  KEY `statId` (`statId`,`periodTypes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryBucketDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryBucketDefinition` (
  `bucketHash` bigint(20) NOT NULL,
  `bucketName` varchar(256) NOT NULL,
  `bucketDescription` varchar(256) NOT NULL,
  `scope` bigint(20) NOT NULL,
  `category` bigint(20) NOT NULL,
  `bucketOrder` bigint(20) NOT NULL,
  `bucketIdentifier` varchar(256) NOT NULL,
  `itemCount` bigint(20) NOT NULL,
  `location` bigint(20) NOT NULL,
  `hasTransferDestination` bigint(20) NOT NULL,
  KEY `bucketHash` (`bucketHash`,`bucketName`,`bucketDescription`,`scope`,`category`,`bucketOrder`,`bucketIdentifier`,`itemCount`,`location`,`hasTransferDestination`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition` (
  `itemHash` bigint(20) NOT NULL,
  `itemName` varchar(256) NOT NULL,
  `itemDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `secondaryIcon` varchar(256) NOT NULL,
  `actionName` varchar(256) NOT NULL,
  `tierTypeName` varchar(256) NOT NULL,
  `tierType` bigint(20) NOT NULL,
  `itemTypeName` varchar(256) NOT NULL,
  `bucketTypeHash` bigint(20) NOT NULL,
  `primaryBaseStatHash` bigint(20) NOT NULL,
  `specialItemType` bigint(20) NOT NULL,
  `talentGridHash` bigint(20) NOT NULL,
  `hasGeometry` bigint(20) NOT NULL,
  `statGroupHash` bigint(20) NOT NULL,
  `qualityLevel` bigint(20) NOT NULL,
  `rewardItemHash` bigint(20) NOT NULL,
  `itemType` bigint(20) NOT NULL,
  `itemSubType` bigint(20) NOT NULL,
  `classType` bigint(20) NOT NULL,
  `nonTransferrable` bigint(20) NOT NULL,
  `exclusive` bigint(20) NOT NULL,
  `maxStackSize` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`itemName`,`itemDescription`,`icon`,`tierTypeName`,`tierType`,`itemTypeName`,`bucketTypeHash`,`primaryBaseStatHash`,`talentGridHash`,`statGroupHash`,`qualityLevel`,`itemType`,`itemSubType`,`classType`,`maxStackSize`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_equippingBlock`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_equippingBlock` (
  `itemHash` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_itemLevels`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_itemLevels` (
  `itemHash` bigint(20) NOT NULL,
  `itemLevels` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`itemLevels`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_perkHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_perkHashes` (
  `itemHash` bigint(20) NOT NULL,
  `perkHashes` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`perkHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_sourceHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_sourceHashes` (
  `itemHash` bigint(20) NOT NULL,
  `sourceHashes` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`sourceHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_stats`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_stats` (
  `itemHash` bigint(20) NOT NULL,
  `statHash` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  `minimum` bigint(20) NOT NULL,
  `maximum` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`statHash`,`value`,`minimum`,`maximum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyInventoryItemDefinition_values`
--

CREATE TABLE IF NOT EXISTS `DestinyInventoryItemDefinition_values` (
  `itemHash` bigint(20) NOT NULL,
  `values` bigint(20) NOT NULL,
  KEY `itemHash` (`itemHash`,`values`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyPlaceDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyPlaceDefinition` (
  `placeHash` bigint(20) NOT NULL,
  `placeName` varchar(256) NOT NULL,
  `placeDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  KEY `placeHash` (`placeHash`,`placeName`,`placeDescription`,`icon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyProgressionDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyProgressionDefinition` (
  `progressionHash` bigint(20) NOT NULL,
  `name` varchar(256) NOT NULL,
  `scope` bigint(20) NOT NULL,
  `repeatLastStep` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `source` varchar(256) NOT NULL,
  KEY `progressionHash` (`progressionHash`,`name`,`scope`,`repeatLastStep`,`identifier`,`description`,`source`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyProgressionDefinition_steps`
--

CREATE TABLE IF NOT EXISTS `DestinyProgressionDefinition_steps` (
  `progressionHash` bigint(20) NOT NULL,
  `progressTotal` bigint(20) NOT NULL,
  KEY `progressionHash` (`progressionHash`,`progressTotal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyRaceDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyRaceDefinition` (
  `raceHash` bigint(20) NOT NULL,
  `raceType` bigint(20) NOT NULL,
  `raceName` varchar(256) NOT NULL,
  `raceNameMale` varchar(256) NOT NULL,
  `raceNameFemale` varchar(256) NOT NULL,
  `raceDescription` varchar(256) NOT NULL,
  KEY `raceHash` (`raceHash`,`raceType`,`raceName`,`raceNameMale`,`raceNameFemale`,`raceDescription`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinySandboxPerkDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinySandboxPerkDefinition` (
  `perkHash` bigint(20) NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `displayDescription` varchar(256) NOT NULL,
  `displayIcon` varchar(256) NOT NULL,
  `isDisplayable` bigint(20) NOT NULL,
  KEY `perkHash` (`perkHash`,`displayName`,`displayDescription`,`displayIcon`,`isDisplayable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinySandboxPerkDefinition_perkGroups`
--

CREATE TABLE IF NOT EXISTS `DestinySandboxPerkDefinition_perkGroups` (
  `perkHash` bigint(20) NOT NULL,
  `weaponPerformance` bigint(20) NOT NULL,
  `impactEffects` bigint(20) NOT NULL,
  `guardianAttributes` bigint(20) NOT NULL,
  `lightAbilities` bigint(20) NOT NULL,
  `damageTypes` bigint(20) NOT NULL,
  KEY `perkHash` (`perkHash`,`weaponPerformance`,`impactEffects`,`guardianAttributes`,`lightAbilities`,`damageTypes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinySpecialEventDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinySpecialEventDefinition` (
  `eventHash` bigint(20) NOT NULL,
  `eventIdentifier` varchar(256) NOT NULL,
  `backgroundImageWeb` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `subtitle` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `progressionHash` bigint(20) NOT NULL,
  `vendorHash` bigint(20) NOT NULL,
  KEY `eventHash` (`eventHash`,`eventIdentifier`,`backgroundImageWeb`,`title`,`subtitle`,`link`,`icon`,`progressionHash`,`vendorHash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyStatDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyStatDefinition` (
  `statHash` bigint(20) NOT NULL,
  `statName` varchar(256) NOT NULL,
  `statDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `statIdentifier` varchar(256) NOT NULL,
  `interpolate` bigint(20) NOT NULL,
  KEY `statHash` (`statHash`,`statName`,`statDescription`,`icon`,`statIdentifier`,`interpolate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyStatGroupDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyStatGroupDefinition` (
  `statGroupHash` bigint(20) NOT NULL,
  `maximumValue` bigint(20) NOT NULL,
  `uiPosition` bigint(20) NOT NULL,
  KEY `statGroupHash` (`statGroupHash`,`maximumValue`,`uiPosition`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyStatGroupDefinition_scaledStats`
--

CREATE TABLE IF NOT EXISTS `DestinyStatGroupDefinition_scaledStats` (
  `statGroupHash` bigint(20) NOT NULL,
  `statHash` bigint(20) NOT NULL,
  `maximumValue` bigint(20) NOT NULL,
  `displayAsNumeric` bigint(20) NOT NULL,
  KEY `statGroupHash` (`statGroupHash`,`statHash`,`maximumValue`,`displayAsNumeric`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyStatGroupDefinition_scaledStats_displayInterpolation`
--

CREATE TABLE IF NOT EXISTS `DestinyStatGroupDefinition_scaledStats_displayInterpolation` (
  `statGroupHash` bigint(20) NOT NULL,
  `statHash` bigint(20) NOT NULL,
  `value` bigint(20) NOT NULL,
  `weight` bigint(20) NOT NULL,
  KEY `statGroupHash` (`statGroupHash`,`statHash`,`value`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition` (
  `gridHash` bigint(20) NOT NULL,
  `maxGridLevel` bigint(20) NOT NULL,
  `gridLevelPerColumn` bigint(20) NOT NULL,
  `progressionHash` bigint(20) NOT NULL,
  `calcMaxGridLevel` bigint(20) NOT NULL,
  `calcProgressToMaxLevel` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`maxGridLevel`,`gridLevelPerColumn`,`progressionHash`,`calcMaxGridLevel`,`calcProgressToMaxLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `row` bigint(20) NOT NULL,
  `columnDGS` bigint(20) NOT NULL,
  `binaryPairNodeIndex` bigint(20) NOT NULL,
  `autoUnlocks` bigint(20) NOT NULL,
  `lastStepRepeats` bigint(20) NOT NULL,
  `isRandom` bigint(20) NOT NULL,
  `isRandomRepurchasable` bigint(20) NOT NULL,
  `randomStartProgressionBarAtProgression` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`row`,`columnDGS`,`binaryPairNodeIndex`,`autoUnlocks`,`lastStepRepeats`,`isRandom`,`isRandomRepurchasable`,`randomStartProgressionBarAtProgression`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_exlusiveWithNodes`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_exlusiveWithNodes` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `exlusiveWithNodes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`exlusiveWithNodes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_prerequisiteNodeIndexes`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_prerequisiteNodeIndexes` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `prerequisiteNodeIndexes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`prerequisiteNodeIndexes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `nodeStepName` varchar(256) NOT NULL,
  `nodeStepDescription` varchar(256) NOT NULL,
  `interactionDescription` varchar(256) NOT NULL,
  `icon` varchar(256) NOT NULL,
  `damageType` bigint(20) NOT NULL,
  `canActivateNextStep` bigint(20) NOT NULL,
  `nextStepIndex` bigint(20) NOT NULL,
  `isNextStepRandom` bigint(20) NOT NULL,
  `startProgressionBarAtProgress` bigint(20) NOT NULL,
  `affectsQuality` bigint(20) NOT NULL,
  `trueStepIndex` bigint(20) NOT NULL,
  `truePropertyIndex` bigint(20) NOT NULL,
  `affectsLevel` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`nodeStepName`,`nodeStepDescription`,`interactionDescription`,`icon`,`damageType`,`canActivateNextStep`,`nextStepIndex`,`isNextStepRandom`,`startProgressionBarAtProgress`,`trueStepIndex`,`truePropertyIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps_actReq`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps_actReq` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `gridLevel` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`gridLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps_actReq_mRH`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps_actReq_mRH` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `gridLevel` bigint(20) NOT NULL,
  `materialRequirementHashes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`gridLevel`,`materialRequirementHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps_perkHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps_perkHashes` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `perkHashes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`perkHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps_statHashes`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps_statHashes` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `statHashes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`statHashes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyTalentGridDefinition_nodes_steps_stepGroups`
--

CREATE TABLE IF NOT EXISTS `DestinyTalentGridDefinition_nodes_steps_stepGroups` (
  `gridHash` bigint(20) NOT NULL,
  `nodeIndex` bigint(20) NOT NULL,
  `nodeHash` bigint(20) NOT NULL,
  `stepIndex` bigint(20) NOT NULL,
  `nodeStepHash` bigint(20) NOT NULL,
  `weaponPerformance` bigint(20) NOT NULL,
  `impactEffects` bigint(20) NOT NULL,
  `guardianAttributes` bigint(20) NOT NULL,
  `lightAbilities` bigint(20) NOT NULL,
  `damageTypes` bigint(20) NOT NULL,
  KEY `gridHash` (`gridHash`,`nodeIndex`,`nodeHash`,`stepIndex`,`nodeStepHash`,`weaponPerformance`,`impactEffects`,`guardianAttributes`,`lightAbilities`,`damageTypes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyUnlockFlagDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyUnlockFlagDefinition` (
  `flagHash` varchar(256) NOT NULL,
  `index` varchar(256) NOT NULL,
  `displayName` varchar(256) NOT NULL,
  `displayDescription` varchar(256) NOT NULL,
  `isOffer` varchar(256) NOT NULL,
  `unlockType` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyVendorDefinition`
--

CREATE TABLE IF NOT EXISTS `DestinyVendorDefinition` (
  `currentLine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyVendorDefinition_categories`
--

CREATE TABLE IF NOT EXISTS `DestinyVendorDefinition_categories` (
  `currentLine` int(11) NOT NULL,
  `categoryHash` bigint(20) NOT NULL,
  `displayTitle` varchar(256) NOT NULL,
  `overlayCurrencyItemHash` bigint(20) NOT NULL,
  `quantityAvailable` bigint(20) NOT NULL,
  `showUnavailableItems` bigint(20) NOT NULL,
  `hideIfNoCurrency` bigint(20) NOT NULL,
  `overlayTitle` varchar(256) NOT NULL,
  `overlayDescription` varchar(256) NOT NULL,
  `overlayChoice` varchar(256) NOT NULL,
  `overlayIcon` varchar(256) NOT NULL,
  `hasOverlay` bigint(20) NOT NULL,
  KEY `currentLine` (`currentLine`,`categoryHash`,`displayTitle`,`overlayCurrencyItemHash`,`quantityAvailable`,`showUnavailableItems`,`hideIfNoCurrency`,`overlayTitle`,`overlayDescription`,`overlayChoice`,`overlayIcon`,`hasOverlay`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DestinyVendorDefinition_summary`
--

CREATE TABLE IF NOT EXISTS `DestinyVendorDefinition_summary` (
  `currentLine` int(11) NOT NULL,
  `vendorHash` bigint(20) NOT NULL,
  `vendorName` varchar(256) NOT NULL,
  `vendorDescription` varchar(256) NOT NULL,
  `vendorIcon` varchar(256) NOT NULL,
  `vendorOrder` bigint(20) NOT NULL,
  `factionName` varchar(256) NOT NULL,
  `factionIcon` varchar(256) NOT NULL,
  `factionHash` bigint(20) NOT NULL,
  `factionDescription` varchar(256) NOT NULL,
  `resetIntervalMinutes` bigint(20) NOT NULL,
  `resetOffsetMinutes` bigint(20) NOT NULL,
  `vendorIdentifier` varchar(256) NOT NULL,
  `positionX` bigint(20) NOT NULL,
  `positionY` bigint(20) NOT NULL,
  `transitionNodeIdentifier` varchar(256) NOT NULL,
  `visible` bigint(20) NOT NULL,
  `progressionHash` bigint(20) NOT NULL,
  `sellString` varchar(256) NOT NULL,
  `buyString` varchar(256) NOT NULL,
  `vendorPortrait` varchar(256) NOT NULL,
  `vendorBanner` varchar(256) NOT NULL,
  `mapSectionIdentifier` varchar(256) NOT NULL,
  `mapSectionName` varchar(256) NOT NULL,
  `mapSectionOrder` bigint(20) NOT NULL,
  `showOnMap` bigint(20) NOT NULL,
  `eventHash` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef`
--

CREATE TABLE IF NOT EXISTS `DGDef` (
  `currentLine` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `themeName` varchar(256) NOT NULL,
  `themeDescription` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_image`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_image` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_image_rect` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_image_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_smallImage` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_smallImage_rect` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_highRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_highRes_smallImage_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_image`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_image` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_image_rect` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_image_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_smallImage` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_smallImage_rect` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_normRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_normRes_smallImage_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `pageName` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_cardBriefs`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_cardBriefs` (
  `currentLine` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `cardId` varchar(256) NOT NULL,
  `points` varchar(256) NOT NULL,
  `totalPoints` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_image`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_image` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_image_rect` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_image_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_smallImage` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_smallImage_rect` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_highRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_highRes_smallImage_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_image`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_image` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_image_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_image_rect` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_image_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_image_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_smallImage`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_smallImage` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `sheetPath` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_smallImage_rect`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_smallImage_rect` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DGDef_tColl_pColl_normRes_smallImage_sheetSize`
--

CREATE TABLE IF NOT EXISTS `DGDef_tColl_pColl_normRes_smallImage_sheetSize` (
  `currentLine` varchar(256) NOT NULL,
  `pageId` varchar(256) NOT NULL,
  `themeId` varchar(256) NOT NULL,
  `x` varchar(256) NOT NULL,
  `y` varchar(256) NOT NULL,
  `height` varchar(256) NOT NULL,
  `width` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


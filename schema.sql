-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: gjallarshift
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

CREATE DATABASE  IF NOT EXISTS `gjallarshift` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gjallarshift`;

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instanceId` varchar(45) NOT NULL,
  `accountId` varchar(45) NOT NULL,
  `characterId` varchar(45) NOT NULL,
  `mode` tinyint(4) NOT NULL,
  `activityDate` datetime NOT NULL,
  `referenceId` varchar(45) NOT NULL,
  `activityName` varchar(50) NOT NULL,
  `assists` smallint(6) DEFAULT '0',
  `score` smallint(6) DEFAULT '0',
  `kills` smallint(6) DEFAULT '0',
  `averageScorePerKill` decimal(7,2) NOT NULL,
  `deaths` smallint(6) DEFAULT '0',
  `averageScorePerLife` decimal(7,2) NOT NULL,
  `completed` tinyint(1) DEFAULT '0',
  `killsDeathsRatio` decimal(5,2) NOT NULL,
  `killsDeathsAssists` decimal(5,2) NOT NULL,
  `activityDurationSeconds` smallint(6) DEFAULT '0',
  `standing` tinyint(1) NOT NULL,
  `team` tinyint(4) NOT NULL,
  `completionReason` tinyint(4) unsigned NOT NULL,
  `fireTeamId` varchar(15) NOT NULL,
  `playerCount` tinyint(4) NOT NULL,
  `hasRewards` tinyint(1) NOT NULL,
  `rewards` text NOT NULL,
  `hasSkulls` tinyint(1) NOT NULL,
  `skulls` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gearset`
--

DROP TABLE IF EXISTS `gearset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gearset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(45) NOT NULL COMMENT 'weapons, armor, items',
  `classType` varchar(45) NOT NULL COMMENT 'Hunter, Titan, Warlock',
  `membershipType` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`userId`,`membershipType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gearset_items`
--

DROP TABLE IF EXISTS `gearset_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gearset_items` (
  `id` int(11) NOT NULL,
  `gearsetId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gearset_item` (`gearsetId`,`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `instanceId` varchar(45) NOT NULL COMMENT 'internal bungie id of YOUR item',
  `itemHash` varchar(45) NOT NULL COMMENT 'bungie id of global item type/class',
  `itemName` varchar(255) NOT NULL COMMENT 'Name of item',
  `itemType` varchar(45) NOT NULL COMMENT 'Primary, Special, Heavy, Subclass, Head, Gauntlets, Chest, Boots, Class, etc',
  `equip` bit(1) NOT NULL COMMENT 'boolean to indicate this is the item of this type that should be equipped',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pholdbox`
--

DROP TABLE IF EXISTS `pholdbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pholdbox` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `session` mediumtext,
  `sessionId` varchar(45) DEFAULT NULL,
  `dateModified` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_idx` (`sessionId`,`dateModified`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membershipId` varchar(255) NOT NULL,
  `activePlatformID` varchar(45) NOT NULL,
  `lastLoginDate` int(11) NOT NULL,
  `destinyAccountInfo` varchar(4000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'gjallarshift'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 19:08:17

# Project GjallarShift - Mobile

Developing a tablet-friendly version of the site


## Setup

[PhoneGap Install Instructions](http://docs.phonegap.com/en/edge/guide_cli_index.md.html#The%20Command-Line%20Interface)

OR

1. [Download and Install Node.js](http://nodejs.org/)
1. Install Cordova run command:
..* Mac: sudo npm install -g cordova
..* Win: npm install -g cordova

## Development

Mobile source code is found in: 
* gjallarshift/mobile/src/gearsets/www

NOTE: The /platforms/ folder is generated, so don't tinker inside that folder

In your terminal:

* cd to `gjallarshift/mobile/src/gearsets`

* compile project to APK: `cordova build`

* test app in emulator: `cordova emulate android`


## Links

* PhoneGap Docs: http://docs.phonegap.com/en/edge/guide_platforms_index.md.html
* Cordova Docs: http://cordova.apache.org/
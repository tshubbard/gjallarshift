var system = require('system');
var page = new WebPage(), testindex = 0, loadInProgress = false;

if (system.args[3] == "xbox") {
    var bungie = "https://www.bungie.net/en/User/SignIn/Wlid?bru=%252f%253flc%253d1033";
} else {
    var bungie = "https://www.bungie.net/en/User/SignIn/Psnid?bru=%252f%253flc%253d1033";
}

page.onConsoleMessage = function(msg) {
  console.log(msg);
};

page.onLoadStarted = function() {
  loadInProgress = true;
};

page.onLoadFinished = function() {
  loadInProgress = false;
};

var steps = [
	function() {
		//Load Login Page
		page.open(bungie);
    
	},
	function() {
		//Enter Credentials
		page.evaluate(function(username, password) {
			document.querySelectorAll("input[type=email]")[0].value = username;
			document.querySelectorAll("input[type=password]")[0].value = password;
			document.querySelectorAll("form")[0].submit();
		}, system.args[1], system.args[2]);
	}, 
	function() {
		// Output content of page to stdout after form has been submitted
		console.log(JSON.stringify(phantom.cookies));
  }
];

interval = setInterval(function() {
	
	if (!loadInProgress && typeof steps[testindex] == "function") {
		steps[testindex]();
		testindex++;
	}
	if (typeof steps[testindex] != "function") {
    	//setTimeout(function(){phantom.exit()}, 5000);
		phantom.exit();
    	clearInterval(interval);
	}
}, 50);

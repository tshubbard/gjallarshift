<?php
/*
 * Created on May 12, 2010
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Destiny Gear Sets</title>
        <link rel="stylesheet" type="text/css" href="/includes/css/vendor/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/includes/css/vendor/vex.css" />
        <link rel="stylesheet" type="text/css" href="/includes/css/vendor/vex-theme-flat-attack.css" />
        <link rel="stylesheet" type="text/css" href="/includes/css/style.css" />
    </head>
    <body ng-app="mainApp">
        <div id="notifications" class="stack"></div>
        <div id="alertWrapper">
            <div id="alerts" ng-controller="AlertCtrl">
                <alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)"
                       ng-class="['alert-' + (alert.type || 'warning'), alert.allowClose ? 'alert-dismissable' : null]">
                    {{alert.msg}}
                    <button ng-show="alert.allowClose" type="button" class="close" ng-click="closeAlert($index)">
                        <span aria-hidden="true">x</span>
                        <span class="sr-only">Close</span>
                    </button>
                </alert>
            </div>
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="#">
                        Project GjallarShift
                      </a>
                </div>
                <div id="navbar" class="navbar-nav navbar-left">
                    <ul class="nav navbar-nav">
                        <li id="loading-gearsets">
                            <a href="/">
                                <img src="/includes/images/loading-nav.gif" alt="Loading" />
                                Loading GearSets
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="navbar" class="navbar-nav navbar-right navbar-collapse collapse">
                  <ul class="nav navbar-nav" ng-controller="NavCtrl">
                    <li class="active"><a href="/">Home</a></li>
                    <li ng-repeat="account in accounts" class="{{account.active}}">
                        <a href="" ng-if="account.type != 'bungie'" ng-click="selectAccount(account.type)">{{account.type}}</a>
                    </li>
                      <li><a href="/main/activities">Activities</a></li>
                      <li><a href="/main/account">My Account</a></li>
                    <?php
                        if($this->getSessionValue("displayName") != ""){
                            echo "<li>" .
                                    "<a href='/main/gearsets'>Gearsets</a>" . 
                                 "</li>";
                        }
                    ?>
                  </ul>
                </div><!--/.navbar-collapse -->
              </div>
        </nav>

        <div id="content">
          <?php include($view);?>
        </div>
        <hr>
        <footer>
            <p>&copy; ParadigmShift <?php echo date('Y'); ?></p>
        </footer>

        <script type="text/javascript" src="/includes/js/vendor/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="/includes/js/vendor/underscore-min.js"></script>
        <script type="text/javascript" src="/includes/js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="/includes/js/vendor/angular.min.js"></script>
        <script type="text/javascript" src="/includes/js/vendor/underscore-min.js"></script>
        <script type="text/javascript" src="/includes/js/app.js"></script>
        <script type="text/javascript" src="/includes/js/services/modal-service.js"></script>
        <script type="text/javascript" src="/includes/js/components/alert-ctrl.js"></script>
        <script type="text/javascript" src="/includes/js/services/itemflow-service.js"></script>
        <script type="text/javascript" src="/includes/js/components/gearset-ctrl.js"></script>
        <script type="text/javascript" src="/includes/js/components/auth-ctrl.js"></script>
        <script type="text/javascript" src="/includes/js/components/nav-ctrl.js"></script>
        <script type="text/javascript" src="/includes/js/services/activity-service.js"></script>
        <script type="text/javascript" src="/includes/js/components/activities-ctrl.js"></script>
        <script src="/includes/js/vendor/vex.combined.min.js"></script>
    </body>
</html>

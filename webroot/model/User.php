<?php
//namespace gjallarshift;
/*
 * Created on 3/6/2015
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class User extends \system\PhORM //<- uncomment to enable ORM
{
    protected $ORM = array("tableName"=>"user",
                           "dsn"=>"",
                           "columns"=>array(
                                "id", 
                                "membershipId", 
                                "activePlatformID",
                                "lastLoginDate",
                                "destinyAccountInfo"),
                           "types"=>array(
                                "int(11)",
                                "varchar(255)",
                                "varchar(45)",
                                "int(11)",
                                "varchar(4000)"),
                           "values"=>array());
}

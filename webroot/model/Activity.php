<?php

//namespace gjallarshift;

class Activity extends \system\PhORM //<- uncomment to enable ORM
{
    protected $ORM = array(
        "tableName" => "activities",
        "dsn" => "",
        "columns" => array(
            "id",
            "accountId",
            "instanceId",
            "characterId",
            "mode",
            "activityDate",
            "referenceId",
            "activityName",
            "assists",
            "score",
            "kills",
            "averageScorePerKill",
            "deaths",
            "averageScorePerLife",
            "completed",
            "killsDeathsRatio",
            "killsDeathsAssists",
            "activityDurationSeconds",
            "standing",
            "team",
            "completionReason",
            "fireTeamId",
            "playerCount",
            "hasRewards",
            "rewards",
            "hasSkulls",
            "skulls",
        ),
        "types" => array(
            "bigint(30)",       // id
            "varchar(45)",      // accountId
            "varchar(45)",      // instanceId
            "varchar(45)",      // characterId
            "tinyint(4)",       // mode
            "datetime",         // activityDate
            "varchar(45)",      // referenceId
            "varchar(50)",      // activityName
            "smallint(6)",      // assists
            "smallint(6)",      // score
            "smallint(6)",      // kills
            "decimal(7, 2)",    // averageScorePerKill
            "smallint(6)",      // deaths
            "decimal(7, 2)",    // averageScorePerLife
            "tinyint(1)",       // completed
            "decimal(5, 2)",    // killsDeathsRatio
            "decimal(5, 2)",    // killsDeathsAssists
            "smallint(6)",      // activityDurationSeconds
            "tinyint(1)",       // standing
            "tinyint(4)",       // team
            "tinyint(4)",       // completionReason
            "varchar(15)",      // fireTeamId
            "tinyint(4)",       // playerCount
            "tinyint(1)",       // hasRewards
            "text",             // rewards
            "tinyint(1)",       // hasSkulls
            "text",             // skulls
        ),
        "values" => array()
    );

    public function parse($item, $defs)
    {
        $defsBlacklist = array(
            'id',
            'accountId',
            'characterId',
            'hasRewards',
            'hasSkulls'
        );

        foreach ($this->ORM["columns"] as $col) {
            if (in_array($col, $defsBlacklist)) {
                continue;
            }

            switch ($col) {
                case 'instanceId':
                case 'referenceId':
                case 'mode':
                    $this->setValue($col, $item->activityDetails->$col);
                    break;

                case 'activityDate':
                    $this->setValue($col, $item->period);
                    break;

                case 'activityName':
                case 'rewards':
                case 'skulls':
                    $refId = $item->activityDetails->referenceId;
                    $activityDef = $defs->activities->$refId;
                    $val = $activityDef->$col;
                    if($col != 'activityName') {
                        $val = json_encode($val);

                    }
                    $this->setValue($col, $val);
                    break;

                default:
                    $this->setValue($col, $item->values->$col->basic->value);
                    break;
            }
        }

        // check rewards and skulls
        $this->checkRewards($this->getValue('rewards'));
        $skulls = json_decode($this->getValue('skulls'));
        $this->setValue('hasSkulls', count($skulls) ? 1 : 0);
    }

    protected function checkRewards($val)
    {
        $val = json_decode($val);
        $hasRewards = false;
        foreach ($val as $rewards) {
            foreach ($rewards->rewardItems as $reward) {
                if ($reward->value != 0) {
                    $hasRewards = true;
                    break;
                }
            }

            if ($hasRewards) {
                break;
            }
        }
        $this->setValue('hasRewards', $hasRewards ? 1 : 0);
    }

    public function toStdClass()
    {
        $data = new stdClass();
        foreach ($this->ORM["columns"] as $col) {
            $data->$col = $this->ORM['values'][$col];
        }
        return $data;
    }
}

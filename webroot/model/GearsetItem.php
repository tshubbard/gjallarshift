<?php
//namespace gjallarshift;
/*
 * Created on 3/6/2015
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class GearsetItem extends \system\PhORM //<- uncomment to enable ORM
{
    protected $ORM = array("tableName"=>"gearset_item",
                           "dsn"=>"",
                           "columns"=>array(
                                "id", 
                                "gearsetId", 
                                "itemId"),
                           "types"=>array(
                                "int(11)",
                                "int(11)",
                                "int(11)"),
                           "values"=>array());
}

<?php
namespace lib;
 
class ServiceResponse
{
    public $success = true;
    public $errors = array();
    public $data = array();
}
?>
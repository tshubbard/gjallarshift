<?php
/*
 * Created on Feb 27, 2015
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
namespace lib;
class BungieAPI extends \system\Model
{
    protected $IOC = array("lib.ServiceResponse");
    
    protected $platforms = array(
        '1' => 'xbox',
        '2' => 'psn',
        '3' => 'blizzard'
    );

    protected $class = array("Titan", "Hunter", "Warlock", "Unknown");
    protected $race = array("Human", "Awoken", "Exo", "Unknown");
    protected $gender = array("Male", "Female", "Unknown");

    /**
     * Post endpoint
     * 
     * @param endpoint endpoint to hit
     * @param data data to post
     * @returns response
     */
    public function post($endpoint, $data)
    {
        $ch = curl_init("https://www.bungie.net/Platform/" . $endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");    
        //curl_setopt($ch, CURLOPT_COOKIE, $this->getSessionValue("cookie"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "X-API-Key: " . $this->SYSTEM["apiKey"],
            "HTTP/1.1 Authorization: Bearer: " . $this->getSessionValue("access_token"),
            "Content-Length: " . strlen($data))
        );
        
        $result = curl_exec($ch);
        return json_decode($result);
    }
    
    /**
     * get endpoint
     * 
     * @param endpoint endpoint to hit
     * @returns response.
     */
    public function get($endpoint, $extravars = "")
    {
    $ch = curl_init("https://www.bungie.net/Platform/" . $endpoint /*. "?" . $this->getSessionValue("bungleloc") . $extravars*/);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");    
        curl_setopt($ch, CURLOPT_COOKIE, $this->getSessionValue("cookie"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "X-API-Key: " . $this->SYSTEM["apiKey"],
            "HTTP/1.1 Authorization: Bearer: " . $this->getSessionValue("access_token")
        ));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * gets or refreshes auth tokens
     *
     * @param [type] $auth
     * @param [type] $grant_type authorization or refresh_token
     * @return void
     */
    public function getToken($auth, $grant_type = "authorization_code")
    {
        $response = $this->instance["lib.ServiceResponse"];
        $data = "grant_type=" . $grant_type . "&code=" . $auth . "&client_id=" . $this->SYSTEM["clientId"];

        try{
            $ch = curl_init("https://www.bungie.net/Platform/app/oauth/token/");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");    
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded",
                "HTTP/1.1 Authorization: Basic: " . $this->SYSTEM["apiKey"],
                "Content-Length: " . strlen($data))
            );
            
            $result = curl_exec($ch);
            $result = json_decode($result);
            if(property_exists($result, "error"))
            {
                $response->success = false;
            }
            array_push($response->data, $result);
        } catch (\Exception $e){

            $response->success = false;
            array_push($response->errors, $e.getMessage());
        }

        return $response;
    }
    
    /**
     * transfer item
     * 
     * Transfers item between vault and character
     * 
     * @param membershipType platform type
     * @param characterid Character id
     * @param itemId Item Id
     * @param itemReferenceHash reference id of item
     * @param stacksize stack size of resources to move...defaults to 1
     * @param toVault boolean indicating direction of transfer.
     * 
     * @returns response
     */
    public function transferItem($membershipType, $characterId, $itemId, $itemReferenceHash, $stackSize, $toVault)
    {
        if ($membershipType == "xbox") {
            $membershipType = 1;
        } else if ($membershipType == "psn") {
            $membershipType = 2;
        }

        $response = $this->instance["lib.ServiceResponse"];
        $response->success = true;
        
        $data = new \stdClass();
        $data->membershipType = $membershipType;
        $data->characterId = $characterId;
        $data->itemId = $itemId;
        $data->itemReferenceHash = $itemReferenceHash;
        $data->stackSize = $stackSize;
        $data->transferToVault = $toVault;
        
        try{
            $result = $this->post("Destiny/TransferItem/", json_encode($data));
            if($result->ErrorCode != 1)
            {
                $response->success = false;
            }
            array_push($response->data, $result);
        } catch (\Exception $e){
            $response->success = false;
            array_push($response->errors, $e.getMessage());
        }
        
        
        
        return $response;
    }
    
    /**
     * transfer item
     * 
     * Transfers item between vault and character
     * 
     * @param membershipType platform type
     * @param characterid Character id
     * @param itemId Item reference hash
     * 
     * @returns response
     */
    public function equipItem($membershipType, $characterId, $itemId)
    {
        $response = $this->instance["lib.ServiceResponse"];
        $response->success = true;
        
        $data = new \stdClass();
        $data->membershipType = $membershipType;
        $data->characterId = $characterId;
        $data->itemId = $itemId;
        
        try{
            $result = $this->post("Destiny/EquipItem/", json_encode($data));
            if($result->ErrorCode != 1)
            {
                $response->success = false;
            }
            array_push($response->data, $result);
        } catch (\Exception $e){
            $response->success = false;
            array_push($response->errors, $e.getMessage());
        }
        
        return $response;
    }
    
    /**
     * Gets bungie ID by membership name (gamertag)
     * 
     * @param $membershipType game system membership (1 = xbox, 2 = ps)
     * @param $displayname gamertag
     */
    public function getMembershipIdByDisplayName($membershipType, $displayName)
    {
        return $this->get("Destiny/" . $membershipType . "/Stats/GetMembershipIdByDisplayName/" . $displayName . "/");
    }
    
    /**
     * Gets all Bungie IDs and membership names (gamertag and psn)
     * 
     */
    public function getAccountInfo()
    {
        $membershipsById = $this->get("User/GetMembershipsById/" . $this->getSessionValue("membership_id") . "/-1/");

        $theArray = array();
        
        foreach($membershipsById->Response->destinyMemberships as $membership) {
            $theID = $membership->membershipId;
            $platformName = "Error - " . $theID . " - ";
            $platform = "Error - " . $theID . " - ";
            $theChars = array();
            $theCharsDetails = array();

            $profile = $this->get("Destiny2/" . $membership->membershipType ."/Profile/". $membership->membershipId . "/?components=200");
            foreach($profile->Response->characters->data as $theChar) {
                $theChars[] = $theChar->characterId;
                $theCharsDetails[$theChar->characterId] = array(
                    "charId" => $theChar->characterId,
                    "class" => $this->class[$theChar->classType],
                    "emblem" => array(
                        "backgroundPath" => $theChar->emblemBackgroundPath,
                        "emblemPath" => $theChar->emblemPath
                    ),
                    "gender" => $this->gender[$theChar->genderType],
                    "level" => $theChar->light,
                    "race" => $this->race[$theChar->raceType],
                );
            }

            $platformName = $membership->displayName;
            $platform = $this->platforms[$membership->membershipType];

            $theArray[$platform] = array(
                "playerID" => $theID,
                "platformID" => $membership->membershipType,
                "playerName" => $platformName,
                "characterIDs" => $theChars,
                "characterDetails" => $theCharsDetails,
            );
        }
        
        $theArray["bungie"] = array(
            "playerName" => $membershipsById->Response->bungieNetUser->displayName,
            "playerID" => $membershipsById->Response->bungieNetUser->membershipId,
            "characterIDs" => array("NA")
        );
        
        return $theArray;

    }

    public function getEntityDefinition($entityType, $hash)
    {
        return $this->get("Destiny2/Manifest/" . $entityType . "/" . $hash . "/");
    }
    
    /**
     * Gets Vault Inventory
     * 
     * @param $membershipType game system membership (1 = xbox, 2 = ps)
     */
    public function getVaultInventory($membershipType)
    {
        $accountInfo = $this->getSessionValue("destinyAccountInfo");
        $destinyMembershipId = $accountInfo[$this->platforms[$membershipType]]["playerID"];
        //just needs to be the first character to get the vault contents;
        $characterId = $accountInfo[$this->platforms[$membershipType]]["characterIDs"][0];
        echo "Destiny2/" . $membershipType . "/Profile/" . $destinyMembershipId . "/?components=201";
        $result = $this->get("Destiny2/" . $membershipType . "/Profile/" . $destinyMembershipId . "/?components=ProfileInventories");
        echo "<pre>" . var_export($result,true) . "</pre>";die();
        return $this->get("Destiny2/" . $membershipType . "/Profile/" . $destinyMembershipId . "/Character/" . $characterId . "/?Components=102");
    }
        
    /*
     * Gets Character Inventory
     * 
     * @param $membershipType game system membership (1 = xbox, 2 = ps)
     * @param $accountID Relevant Account ID
     * @param $characterID Relevant Character ID
     */
    public function getCharacterInventory($membershipType, $accountID, $characterID)
    {
        return $this->get("Destiny/" . $membershipType . "/Account/" . $accountID . "/Character/" . $characterID . "/Inventory/", "&definitions=true");
    }

    public function getActivities($membershipType, $accountID, $characterID, $mode = 0, $count = 20) {
        $params = "lc=en&fmt=true&lcin=true&mode=" . $mode . "&count=" . $count . "&page=0&definitions=true";
        return $this->get("Destiny/Stats/ActivityHistory/" . $membershipType . "/" . $accountID . "/" . $characterID . '/', $params);
    }
}


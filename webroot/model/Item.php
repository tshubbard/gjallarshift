<?php
//namespace gjallarshift;
/*
 * Created on 3/6/2015
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class Item extends \system\PhORM //<- uncomment to enable ORM
{
    protected $ORM = array("tableName"=>"item",
                           "dsn"=>"",
                           "columns"=>array(
                                "id", 
                                "instanceId", 
                                "itemHash",
                                "itemName",
                                "itemType",
                                "equip"),
                           "types"=>array(
                                "int(11)",
                                "varchar(45)",
                                "varchar(45)",
                                "varchar(255)",
                                "bit(1)"),
                           "values"=>array());
}

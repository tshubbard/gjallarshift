<?php
//namespace gjallarshift;
/*
 * Created on 3/6/2015
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class Gearset extends \system\PhORM //<- uncomment to enable ORM
{
    protected $ORM = array("tableName"=>"gearset",
                           "dsn"=>"",
                           "columns"=>array(
                                "id", 
                                "userId", 
                                "name",
                                "type",
                                "classType",
                                "membershipType"),
                           "types"=>array(
                                "int(11)",
                                "int(11)",
                                "varchar(255)",
                                "varchar(45)",
                                "varchar(45)",
                                "int(11)"),
                           "values"=>array());
}

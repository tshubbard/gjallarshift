"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app'),
    appModule = angular.module(appName);
/**
 * AlertCtrl handles DOM manipulation driven by ModalService
 */
appModule.controller('AlertCtrl', ['$scope', 'ModalService',
    function AlertCtrl($scope, ModalService) {
        $scope.alerts = ModalService.alerts;
        $scope.closeAlert = ModalService.closeAlert;
    }
]);

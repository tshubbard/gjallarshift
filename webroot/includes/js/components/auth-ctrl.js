"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app');
var appModule = angular.module(appName);

appModule.controller('AuthCtrl', ['$scope', '$http', 'ModalService',
	function AuthCtrl($scope, $http, ModalService) {
		// enables/disables Register/Login buttons
		$scope.isProcessing = false;
		
		$scope.clientId = "";

		$scope.getClientId = function(){
			$http.get("/main/getClientId").then(function(response){
				$scope.clientId = response.data.clientId;
			});
		}

		$scope.bungieAuth = function() {
			$scope.isProcessing = true;
			ModalService.startLoading({
				title: 'Logging In...',
				msg: "Attempting to login to Bungie..."
			});

			window.location = 'https://www.bungie.net/en/oauth/authorize?client_id=' + $scope.clientId + '&response_type=code';
			/*var config = {
				//withCredentials: true,
				params: {
					username: encodeURIComponent($('#username').val()),
					password: encodeURIComponent($('#password').val()),
					platformName: encodeURIComponent($('input[name="platformName"]:checked').val())
				}
			};
			
			var params = {
				username: encodeURIComponent($('#username').val()),
				password: encodeURIComponent($('#password').val()),
				platformName: encodeURIComponent($('input[name="platformName"]:checked').val())
			};

			
			//switched to jquery because angular is stupid and can't send proper post vars.
			$.ajax({
				url: "/?event=main.auth",
				method: "POST",
				data: params
			}).success(function(data, status, headers, config) {
				ModalService.stopLoading();

				// this callback will be called asynchronously
			    // when the response is available
			    if (data.success) {
			        $scope.$root.$broadcast("auth:bungieAuth");
			    } else {
					var msgs = '';
			    	for (var error in data.errors) {
						msgs += data.errors[error] + ". ";
			    	}

					ModalService.alert({
						title: 'Login Errors',
						msg: msgs,
						type: 'danger'
					});
			    }
			}).error(function(data, status, headers, config) {
				$scope.isProcessing = false;
				ModalService.stopLoading(function() {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					ModalService.alert({
						title: 'Login Errors',
						msg: 'Something went wrong...' + data,
						type: 'danger'
					});
				});
			});*/
		};
		
		//Registers
		$scope.register = function() {
			var loginObj = $scope.checkLogin();
			if(loginObj.hasErrors) {
				return;
			}

		    var params = {
                username: loginObj.userVal,
                password: loginObj.passVal
            };

			$scope.isProcessing = true;

			$.ajax({
				url: "?event=users.register",
				method: "POST",
				data: params
			}).success(function(data){
				if(data.success){
					ModalService.showModal({
						type: 'success',
						title: 'Registration Success',
						msg: 'Registered with DestinyGearsets! Continuing to site...',
						closeAfter: 2000,
						afterClose: function() {
							$scope.login();

						}
					});
				} else {
					ModalService.showModal({
						type: 'danger',
						title: 'Registration Error',
						msg: data.errors[0],
						afterClose: function() {
							$scope.isProcessing = false;
							$scope.$apply();
						}
					});
				}
			});
		}

		//Logs In
		$scope.login = function() {
			var loginObj = $scope.checkLogin();
			console.log('login!: ', loginObj);

			if(loginObj.hasErrors) {
				return;
			}

			var params = {
				username: loginObj.userVal,
				password: loginObj.passVal
			};

			$scope.isProcessing = true;

			$.ajax({
				url: "?event=users.login",
				method: "POST",
				data: params
			}).success(function(data){
				if(data.success) {
					window.location = "?event=main.gearsets";
				} else {
					ModalService.showModal({
						type: 'danger',
						title: 'Login Error',
						msg: 'Username and Password combination invalid.',
						afterClose: function() {
							$scope.isProcessing = false;
							$scope.$apply();
						}
					});
				}
			});
        }

		$scope.checkLogin = function() {
			var $userEl = $('#username'),
				$userVal = $userEl.val(),
				$userErrorEl = $('.username-error-msg'),
				$passEl = $('#password'),
				$passVal = $passEl.val(),
				$passErrorEl = $('.password-error-msg'),
				hasError = false;

			$userErrorEl.hide();
			$passErrorEl.hide();
			$userEl.removeClass('errorState');
			$passEl.removeClass('errorState');

			if(!$userVal.length) {
				$('.username-error-msg').show();
				$userEl.addClass('errorState');
				hasError = true;
			}

			if(!$passVal.length) {
				$('.password-error-msg').show();
				$passEl.addClass('errorState');
				hasError = true;
			}

			return {
				hasErrors: hasError,
				userVal: $userVal,
				passVal: $passVal
			}
		}
		$scope.getClientId();
	}
]);

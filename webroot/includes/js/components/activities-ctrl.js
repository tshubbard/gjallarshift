"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app'),
    appModule = angular.module(appName);
/**
 * AlertCtrl handles DOM manipulation driven by ModalService
 */
appModule.controller('ActivitiesCtrl', ['$scope', '$http', 'ActivityService',
    function ActivitiesCtrl($scope, $http, ActivityService) {
        $scope.activities = ActivityService.getActivities('2305843009216266818', ActivityService.MODE_RAID, 10);
    }
]);

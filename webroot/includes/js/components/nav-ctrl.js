"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app'),
    appModule = angular.module(appName);

appModule.controller('NavCtrl', ['$scope', '$http',
    function NavCtrl($scope, $http) {
        // initialize accounts
        $scope.accounts = [];
        
        //get account info
        $scope.getAccountInfo = function(){
            var config = {
                    params: {
                        event: 'main.accounts'
                    }
                }
                
            $http.get('/', config).success(function(data, status, headers, config) {
                if(data.success){
                    $scope.accounts = data.data;
                }
            });
        }
        
        $scope.logout = function(){
            window.location = "/?event=users.logout"
        }
        
        //select active account
        $scope.selectAccount = function(account){
            $scope.$root.$broadcast('platform:change', account);
        }
        
        $scope.$on('auth:bungieAuth', function() {
            $scope.getAccountInfo();
        });
        
        //initial load
        // get accounts on load
        $scope.getAccountInfo();
    }
]);

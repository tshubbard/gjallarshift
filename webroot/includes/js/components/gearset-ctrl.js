    "use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app');
var appModule = angular.module(appName);

appModule.controller('GearSetCtrl', ['$scope', '$http', 'ItemFlowService', 'ModalService',
	function GearSetCtrl($scope, $http, ItemFlowService, ModalService) {
		// initialize gearsets
		$scope.gearsets = [];
		$scope.armorgearsets = [];
		// initialize items
		$scope.items = [];
		// initialize vault slot stats;
		$scope.vault = [];
		// initialize player slot stats;
		$scope.player = [];
		// initialize itemindex
		$scope.itemIndex = [];
		// initialize characters
		$scope.characters = [];

		var gearsetCount = 0;

		// Get item list
		$scope.getItems = function() {
			var showAlert = false;
			if (typeof(localStorage["inventory"]) === 'undefined' || typeof(localStorage["inventoryIndex"]) === 'undefined' || typeof(localStorage["playerSlots"]) === 'undefined' || typeof(localStorage["vaultSlots"]) === 'undefined') {
				showAlert = true;
				ModalService.startLoading({
					title: 'Loading GearSets...'
				});
			} else {
				$('#loading-gearsets').show();
				// pull cached inventory objects for quick access
				$scope.items = JSON.parse(localStorage["inventory"]);
				$scope.itemIndex = JSON.parse(localStorage["inventoryIndex"]);
				$scope.player = JSON.parse(localStorage["playerSlots"]);
				$scope.vault = JSON.parse(localStorage["vaultSlots"]);
			}
			
			$http.get('/api/getInventory').success(function(data, status, headers, config) {
				if (showAlert) {
					ModalService.stopLoading();
				} else {
					$('#loading-gearsets').hide();
				}
				
	    		// this callback will be called asynchronously
	    		// when the response is available
	    		if (data.success) {
	    		   
	    			// create associative array for items
					/*var tmpItemObj = {};
	    			for (var itemObj in data.data[0].items) {
	    				var item = data.data[0].items[itemObj];
	    				tmpItemObj[item.itemInstanceId] = itemObj;
	    			}*/
	    			$scope.items = data.data[0].items;
	    			$scope.vault = data.data[0].vault;
	    			$scope.player = data.data[0].player;
	    			$scope.itemIndex = data.data[0].itemIndex;
	    			//$scope.itemIndex = tmpItemObj;
	    			console.log($scope.vault);
	    			console.log($scope.player);
	    			console.log($scope.itemIndex);
	    			console.log($scope.items);

	    			// save to localstorage to cache items
	    			localStorage["inventory"] = JSON.stringify(data.data[0].items);
	    			localStorage["inventoryIndex"] = JSON.stringify(data.data[0].itemIndex);
	    			localStorage["playerSlots"] = JSON.stringify(data.data[0].player);
	    			localStorage["vaultSlots"] = JSON.stringify(data.data[0].vault);

	    			$('#loading-gearsets').hide();
	    		} else {
	    		    //need to log in again
	    		    $('#bungie-modal').modal('show');
	    		}

	  		}).error(function(data, status, headers, config) {	  		    
	    		// called asynchronously if an error occurs
	    		// or server returns response with an error status.
	  		});
	  	};

	  	// listen for reloading of gearsets
	  	$scope.$on('reload:gearsets', function() {
            $scope.getItems();
        });
	  	
	  	//get character info
	  	$scope.getCharacterInfo = function(platform){
	  	    var config = {
	  	            params: {
	                    platform: platform
	                }
	            }
	            
            $http.get('/main/characters', config).success(function(data, status, headers, config) {
                if(data.success){
                    $scope.characters = data.data;
                }
            });
	  	}
	  	
	  	$scope.$on('auth:bungieAuth', function() {
	  	    $scope.getItems();
	  	    $scope.getCharacterInfo();
	  	});
	  	
	  	$scope.$on("platform:change", function(event, data){
	  	    $scope.getCharacterInfo(data);
	  	});
	  	
		// initialize selecting of an item
		$scope.selectItem = function(panelType, panelId, additionalSlotId) {
			var activePanel;
			if (panelType === "weapons") {
				activePanel = $scope.activeGearSet;
				// remove active status on armor panel
				if (typeof($scope.activeArmorGearSet) !== 'undefined') {
					for (var p in $scope.activeArmorGearSet.panels) {
						var panel = $scope.activeArmorGearSet.panels[p];
						panel.active = false;
					}
				}
			} else if (panelType === "armor") {
				activePanel = $scope.activeArmorGearSet;
				// remove active status on weapons panel
				if (typeof($scope.activeGearSet) !== 'undefined') {
					for (var p in $scope.activeGearSet.panels) {
						var panel = $scope.activeGearSet.panels[p];
						panel.active = false;
					}
				}
			}
			for (var panelItem in activePanel.panels) {
				var panel = activePanel.panels[panelItem];

				if (panel.id === panelId) {
					panel.active = true;
					// items in panel
					var itemsInPanel = [];

					// add main item if exists
					if (typeof(panel.item) !== 'undefined') {
						itemsInPanel.push(panel.item);
					}

					// loop through the panels additional slots to find the active one
					for (var additionalSlotItem in panel.additionalSlots) {
						var additionalSlot = panel.additionalSlots[additionalSlotItem];
						if (additionalSlot.id === additionalSlotId) {
							additionalSlot.active = true;
						} else {
							additionalSlot.active = false;
						}

						// push item to array if exists
						if (typeof(additionalSlot.item) !== 'undefined') {
							itemsInPanel.push(additionalSlot.item);
						}
					}

					// loop items to only show items with that type
					for (var itemIndex in $scope.items) {
						var item = $scope.items[itemIndex];
						item.hide = true;
						// check to see if the item type matches the allowed types in the panel
						for (var itemTypeName in panel.itemTypeNameList) {
							var allowedItemType = panel.itemTypeNameList[itemTypeName];
							if (allowedItemType === item.itemTypeName) {
								item.hide = false;

								// check if an item has already been selected for this panel
								for (var itemIdIndex in itemsInPanel) {
									var itemId = itemsInPanel[itemIdIndex];
									if (item.itemInstanceId === itemId) {
										item.hide = true;
									}
								}

								/*if (typeof(panel.item) !== 'undefined') {
									if (panel.item !== item.itemInstanceId) {
										item.hide = false;	
									}
								} else {
									item.hide = false;
								}*/
								
								break;
							}
						}
					}
				} else {
					panel.active = false;
				}
			}

			$('#item-list').css("width", "300px");
		};

		// move a selected item to gearset
		$scope.moveItemToGearSet = function(itemInstanceId) {
			if (typeof($scope.activeGearSet) !== 'undefined') {
				for (var panelItem in $scope.activeGearSet.panels) {
					var panel = $scope.activeGearSet.panels[panelItem];
					if (panel.active) {
						// if the additional item slot is active, set the item there instead
						var isAdditionalItem = false;
						for (var additionalSlotItem in panel.additionalSlots) {
							var additionalSlot = panel.additionalSlots[additionalSlotItem];
							if (additionalSlot.active) {
								isAdditionalItem = true;
								additionalSlot.item = itemInstanceId;
								additionalSlot.active = false;
							}
						}
						// set item from items list as the panel item
						if (!isAdditionalItem) {
							panel.item = itemInstanceId;
						}
						
						// hide item from item list since it is being used
						//$scope.items[itemIndex].hide = true;

						panel.active = false;
						break;
					}
				}
			}

			if (typeof($scope.activeArmorGearSet) !== 'undefined') {
				for (var panelItem in $scope.activeArmorGearSet.panels) {
					var panel = $scope.activeArmorGearSet.panels[panelItem];
					if (panel.active) {
						// if the additional item slot is active, set the item there instead
						var isAdditionalItem = false;
						for (var additionalSlotItem in panel.additionalSlots) {
							var additionalSlot = panel.additionalSlots[additionalSlotItem];
							if (additionalSlot.active) {
								isAdditionalItem = true;
								additionalSlot.item = itemInstanceId;
								additionalSlot.active = false;
							}
						}
						// set item from items list as the panel item
						if (!isAdditionalItem) {
							panel.item = itemInstanceId;
						}

						// hide item from item list since it is being used
						//$scope.items[itemIndex].hide = true;

						panel.active = false;
						break;
					}
				}
			}
			
			$('#item-list').css("width", "0");
		};

		// fill gearset array with test data
		for (var i = 0; i < 4; i++) {
			$scope.gearsets.push(fillGearSet());
			$scope.armorgearsets.push(fillArmorGearSet());
		}

		// add new gearset to the page
		$scope.showNewGearSet = function() {
			$scope.addGearSet = true;
		};

		// add the new gearset to the stack
		$scope.addNewGearSet = function(gearsetName) {
			$scope.gearsets.push(fillGearSet(gearsetName));
			$scope.addGearSet = false;
		};

		// set the active gearset for configuration
		$scope.configureGearSet = function(gearsetId) {
			console.log(gearsetId);
			// loop gearsets to set active
			for (var gearsetItem in $scope.gearsets) {
				var gearset = $scope.gearsets[gearsetItem];
				if (gearset.id === gearsetId) {
					gearset.active = true;
					$scope.activeGearSet = gearset;
				} else {
					gearset.active = false;
				}
			}
		};

		// set the active armor gearset for configuration
		$scope.configureArmorGearSet = function(gearsetId) {
			console.log(gearsetId);
			// loop gearsets to set active
			for (var gearsetItem in $scope.armorgearsets) {
				var gearset = $scope.armorgearsets[gearsetItem];
				if (gearset.id === gearsetId) {
					gearset.active = true;
					$scope.activeArmorGearSet = gearset;
				} else {
					gearset.active = false;
				}
			}
		};

		// start equipping the gearset
		$scope.equipGearSet = function() {
			var mergeGearSets = $scope.activeGearSet.panels.concat($scope.activeArmorGearSet.panels);
			//console.log(mergeGearSets);

			$scope.itemFlowQueue = ItemFlowService.buildItemFlowQueue($scope.activeCharacter, mergeGearSets, $scope.items, $scope.itemIndex, $scope.player, $scope.vault);
			// Show Item Flow
			$('#itemflow-modal').modal('show');

			// show item flow object
			//console.log($scope.itemFlowQueue);
			//console.log('START QUEUE');
			ItemFlowService.processItemFlowQueue($scope.itemFlowQueue);
		};
		
		// get items on page load
		$scope.getItems();
		
		// get characters on load
		$scope.getCharacterInfo();
		
		// on active character change
		$('input[name="activeCharacter"]').change(function() {
			$scope.activeCharacter = $(this).val();
		});

		// fill gearset with data
		function fillGearSet(gearsetName) {
			gearsetCount++;
			return {
				id: gearsetCount,
				name: typeof(gearsetName) !== 'undefined' ? gearsetName : 'New GearSet ' + gearsetCount,
				panels: [
					new itemGearSetPanel('primary-weapon', 'Primary Weapon', 9, ['Auto Rifle', 'Scout Rifle', 'Hand Cannon', 'Pulse Rifle']),
					new itemGearSetPanel('secondary-weapon', 'Secondary Weapon', 9, ['Shotgun', 'Fusion Rifle', 'Sniper Rifle']),
					new itemGearSetPanel('heavy-weapon', 'Heavy Weapon', 9, ['Machine Gun', 'Rocket Launcher'])
				]
			};
		}

		// fill gearset with data
		function fillArmorGearSet(gearsetName) {
			gearsetCount++;
			return {
				id: gearsetCount,
				name: typeof(gearsetName) !== 'undefined' ? gearsetName : 'New GearSet ' + gearsetCount,
				panels: [
					new itemGearSetPanel('head', 'Head', 9, ['Helmet']),
					new itemGearSetPanel('gloves', 'Gloves', 9, ['Gauntlets']),
					new itemGearSetPanel('chest', 'Chest', 9, ['Chest Armor']),
					new itemGearSetPanel('boots', 'Boots', 9, ['Leg Armor']),
					new itemGearSetPanel('class', 'Class', 0, ['Warlock Subclass', 'Titan Subclass', 'Hunter Subclass'])
				]
			};
		}

		// item gearset panels
		function itemGearSetPanel(panelId, panelName, numAdditionalSlots, itemTypeNameList) {
			var additionalSlots = [];
			for (var i = 0; i < numAdditionalSlots; i++) {
				additionalSlots.push(new itemGearSetAdditionalSlot(panelId + '-additional-' + i));
			}
			return {
				id: panelId,
				name: panelName,
				itemTypeNameList: itemTypeNameList,
				additionalSlots: additionalSlots,
				active: false
			};
		}

		// additional slots per panel
		function itemGearSetAdditionalSlot(additionalPanelId) {
			return {
				id: additionalPanelId,
				active: false
			}
		}

	}
]);

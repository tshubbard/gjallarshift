"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app');
var appModule = angular.module(appName);

appModule.service('ActivityService', ['$http', '$rootScope',
    function ActivityService ($http, $rootScope) {
        this.MODE_ALL           = 0;
        this.MODE_STORY         = 2;
        this.MODE_STRIKE        = 3;
        this.MODE_RAID          = 4;
        this.MODE_ALL_PVP       = 5;
        this.MODE_PATROL        = 6;
        this.MODE_ALL_PVE       = 7;
        this.MODE_PVP_INTRO     = 8;
        this.MODE_3V3           = 9;
        this.MODE_CONTROL       = 10;
        this.MODE_LOCKDOWN      = 11;
        this.MODE_TEAM          = 12;
        this.MODE_FFA           = 13;
        this.MODE_OSIRIS        = 14;
        this.MODE_DOUBLES       = 15;
        this.MODE_NIGHTFALL     = 16;
        this.MODE_HEROIC        = 17;
        this.MODE_ALL_STRIKES   = 18;
        this.modeToLabelMap = {
            0:  "None",
            2:  "Story",
            3:  "Strike",
            4:  "Raid",
            5:  "AllPvP",
            6:  "Patrol",
            7:  "AllPvE",
            8:  "PvPIntroduction",
            9:  "ThreeVsThree",
            10: "Control",
            11: "Lockdown",
            12: "Team",
            13: "FreeForAll",
            14: "TrialsOfOsiris",
            15: "Doubles",
            16: "Nightfall",
            17: "Heroic",
            18: "AllStrikes"
        };
        this.labelToModeMap = {
            "None": 0,
            "Story": 2,
            "Strike": 3,
            "Raid": 4,
            "AllPvP": 5,
            "Patrol": 6,
            "AllPvE": 7,
            "PvPIntroduction": 8,
            "ThreeVsThree": 9,
            "Control": 10,
            "Lockdown": 11,
            "Team": 12,
            "FreeForAll": 13,
            "TrialsOfOsiris": 14,
            "Doubles": 15,
            "Nightfall": 16,
            "Heroic": 17,
            "AllStrikes": 18
        };

        this.getActivities = function(charId, mode, count) {
            var config = {
                params: {
                    characterId: charId,
                    mode: mode || 0,
                    count: count || 20
                }
            };

            console.log('ActivityService Params: ', config);

            $http.get('/?event=api.getActivities', config).success(function(data, status, headers, config) {
                console.log('ActivityService Success! Data: ', data);
            }).error(function(data, status, headers, config) {
                console.log('ActivityService ERROR! Data: ', data);
            });
        }
    }
]);

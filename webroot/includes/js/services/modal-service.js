"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app'),
    appModule = angular.module(appName);

/**
 * ModalService wraps two concepts:
 *      "alerts": minimalistic one to two line notices for the user. These pop up with bootstrap styles and are
 *                not UI-blocking. The user can close the alerts or leave them at the top and continue in the app
 *      "modals": A UI-blocking overlay with pretty content
 *
 * Alert Options:
 *      msg {String} The message content for the alert
 *      type {String} The alert "type", default 'warning':
 *          'success'   green
 *          'info'      blue
 *          'warning'   yellow
 *          'danger'    red
 *      allowClose {Boolean} If the user can close the alert, default true
 *
 * Modal Main Options:
 *      title {string} The title for the alert modal
 *      msg {string} The body/message for the alert modal
 *      type {string} The 'type' of alert modal, currently only `type: 'error'` is used. Changes CSS class for alert
 *      hideAfter {int} Hide the alert after this time in ms: ex. hideAfter: 3000 hides the alert after 3 seconds
 *      allowClose {Boolean} Allow the user to close the modal, default: true
 *      afterOpen {Function} Callback when modal is done showing
 *      afterClose {Function} Callback when modal is done closing/hiding
 *
 * Modal Other Options
 *      content {String} Everything inside the modal, all HTML (except the close button), text, etc.
 *          If empty, `content` is built by templates for the `title` and `msg` options
 *      showCloseButton {Boolean} To show the close button or not, use `allowClose`, default: true
 *      escapeButtonCloses {Boolean} If the user can press the Escape key to close the modal, default: true
 *      overlayClosesOnClick {Boolean} If clicking the overlay closes modal, default: true
 *      className {String} Any CSS classes you want to add to the default CSS className
 *      css {Object} Lets you manaully set css properties
 *      overlayClassName {String} Sets the overlay's class
 *      overlayCSS {Object} CSS for the overlay background
 *      contentClassName {String} Sets the content's class
 *      contentCSS {Object} CSS for the actual content
 *      closeClassName {String} Sets the close button's class
 *      closeCSS {Object} CSS for the close button
 */
app.service('ModalService', [ '$controller', '$timeout',
    function ModalService($controller, $timeout) {
        // set vex's default class
        vex.defaultOptions.className = 'vex-theme-flat-attack';

        this.defaultVexOptions = {
            content: '',
            allowClose: true,
            showCloseButton: true,
            escapeButtonCloses: true,
            overlayClosesOnClick: true,
            appendLocation: '#notifications',
            className: 'vex-theme-flat-attack',
            css: {},
            overlayClassName: '',
            overlayCSS: {},
            contentClassName: '',
            contentCSS: {},
            closeClassName: '',
            closeCSS: {}
        };

        this.defaultAlertOptions = {
            msg: '',
            type: '',
            allowClose: true
        };

        this.alerts = [];

        /**
         * Adds an Alert notice to the page
         *
         * @param options {Object} Alert Options
         */
        this.alert = function(options) {
            options = _.extend(_.clone(this.defaultAlertOptions), options);
            this.alerts.push(options);

            if(options.closeAfter) {
                if(options.closeAfter) {
                    $timeout(_.bind(function() {
                        this.closeAlert(this.alerts.length - 1);
                    }, this), options.closeAfter);
                }
            }
        };

        /**
         * Closes an alert by index
         *
         * @param index {int} The Alert index
         */
        this.closeAlert = function(index) {
            console.log('closeAlert ', index);
            this.alerts.splice(index, 1);
        };

        /**
         * Shows the modal overlay in the browser
         *
         * @param options {Object} Modal Options
         */
        this.showModal = function(options) {
            if(options.className) {
                options.className = this.defaultVexOptions.className + ' ' + options.className;
            }

            options = _.extend(_.clone(this.defaultVexOptions), options);
            options.content = this._buildVexContent(options, false);

            this._showModal(options);
        };

        /**
         * Wraps showModal adding the loading spinner template
         *
         * @param options {Object} Modal Options
         */
        this.startLoading = function(options) {
            // users should have to wait until loading finishes
            options.allowClose = false;
            options = _.extend(_.clone(this.defaultVexOptions), options);
            options.content = this._buildVexContent(options, true);

            this._showModal(options);
        };

        /**
         * Stops the loading modal
         *
         * @param onComplete {Function} Callback when modals are closed
         */
        this.stopLoading = function(onComplete) {
            if(_.isFunction(onComplete)) {
                var $bodyEl = $('body');
                $bodyEl.on('vexAfterClose', function(opts) {
                    $bodyEl.off('vexAfterClose');
                    onComplete();
                });
            }

            vex.closeAll();
        };

        /**
         * Calls Vex to display the Modal
         *
         * @param options {Object} Modal Options
         * @private
         */
        this._showModal = function(options) {
            if(!options.allowClose) {
                // if we don't allow close, dont show the close button
                // or allow clicks or Escape key to close either
                options.showCloseButton = false;
                options.escapeButtonCloses = false;
                options.overlayClosesOnClick = false;
            }

            var $vexContent = vex.open(options);

            if(options.closeAfter) {
                $timeout(function() {
                    vex.close($vexContent.data().vex.id);
                }, options.closeAfter)
            }
        };

        /**
         * Builds the Vex content property from the options
         *
         * @param options {Object} Modal Options
         * @param addLoadingGif {Boolean} If we should add the loading gif
         * @returns {string} HTML for the vex content property
         * @private
         */
        this._buildVexContent = function(options, addLoadingGif) {
            var html = '';
            if(options.content) {
                // if content is passed in, use that first
                html += options.content;
            } else {
                // otherwise build title and message if available
                if(options.title) {
                    html += this._titleTpl(options);
                }
                if(options.msg) {
                    html += this._bodyTpl(options);
                }
            }

            if(addLoadingGif) {
                // add loading gif if requested
                html += this._loadingTpl(options);
            }

            return html;
        };

        this._loadingTpl = function(options) {
            return  '<div class="loadingImage">' +
                        '<img src="/includes/images/loading.gif" alt="Loading..." />' +
                    '</div>';
        };

        this._titleTpl = function(options) {
            return '<div class="modal-header"><h4 class="modal-title">' + options.title + '</h4></div>';
        };

        this._bodyTpl = function(options) {
            var alertClass = options.type ? 'alert-' + options.type : '';
            return '<div class="modal-body"><div class="alert ' + alertClass + '">' + options.msg + '</div></div>';
        };
    }
]);

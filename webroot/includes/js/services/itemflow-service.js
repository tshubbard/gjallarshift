"use strict";

var appName = angular.element($('[ng-app]')).attr('ng-app');
var appModule = angular.module(appName);

appModule.service('ItemFlowService', ['$http', '$rootScope',
	function ItemFlowService($http, $rootScope) {

		var _characterId, _itemFlowQueue, _queue, 
			_inventory, _inventoryIndex, _playerSlots, 
			_vaultSlots;
		
		this.buildItemFlowQueue = function(charId, loadoutObj, inventoryObj, itemIndex, playerSlots, vaultSlots) {
			_characterId = charId;
			_inventory = inventoryObj;
			_inventoryIndex = itemIndex;
			_playerSlots = playerSlots;
			_vaultSlots = vaultSlots;

			// get items to equip
			return getItemFlowQueue(loadoutObj);
		};

		this.processItemFlowQueue = function(itemFlowQueueList) {
			_itemFlowQueue = itemFlowQueueList;

			// start the queue
			processQueueItem();
		};

		// process the item flow queue recursively
		function processQueueItem() {
			// define variable to store the found queue item to process
			var handleQueueItem = null;

			for (var item in _itemFlowQueue) {
				var queueItem = _itemFlowQueue[item];

				// if not complete and not error then process this item
				if (!queueItem.complete && !queueItem.error) {
					handleQueueItem = queueItem;
					break;
				}
			}

			// no items to process found
			if (handleQueueItem === null) {
				return;
			}

			$.ajax({
				url: handleQueueItem.endpoint.url,
				method: "POST",
				data: handleQueueItem.endpoint.params
			}).success(function(data, status, headers, config) {
			    // this callback will be called asynchronously
			    // when the response is available
			    console.log(data);
			    if (data.success) {
			    	handleQueueItem.complete = true;
			    	handleQueueItem.status = 'success';
			    } else {
			    	handleQueueItem.error = true;
			    	handleQueueItem.status = 'danger';
			    }

			    $rootScope.$apply();

			    // if all items completed successfully, hide the modal
			    var fullyCompleted = true;
			    for (var item in _itemFlowQueue) {
					var queueItem = _itemFlowQueue[item];
					if (!queueItem.complete) {
						fullyCompleted = false;
					}
				}

				// if ItemFlow completed successfully
				if (fullyCompleted) {
					$('#itemflow-modal').modal('hide');
					// reload gearsets
					$rootScope.$broadcast('reload:gearsets');
				}

			    // handle next item in queue
			    setTimeout(processQueueItem, 1000);

			}).error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			    handleQueueItem.error = true;
			    handleQueueItem.status = 'danger';

			    $rootScope.$apply();

			    // handle next item in queue
			    setTimeout(processQueueItem, 1000);
			});
		}

		// iterate over items, if they are not already on the active character, add them to be equipped
		function getItemFlowQueue(items) {
			// reset queue
			_queue = [];


			for (var loadoutItem in items) {
				if (typeof(items[loadoutItem].item) === 'undefined') {
					continue;
				}
				
				var item = _inventory[_inventoryIndex[items[loadoutItem].item]];

				// is the item in the vault?
				if (item.inVault) {
					// QUEUE TO MOVE ITEM FROM VAULT TO CHARACTER
					moveItemFromVault(_characterId, item, "Move Item from Vault to Character");

					// QUEUE TO EQUIP ITEM
					equipItemOnCharacter(_characterId, item.itemInstanceId, item, "Equip Item on Character");
					continue;
				}

				// is the item not equipped but on the character for the loadout?
				if (!item.equipped && _characterId === item.characterId) {
					// QUEUE TO EQUIP ITEM ON CHARACTER
					equipItemOnCharacter(_characterId, item.itemInstanceId, item, "Equip Item on Character");
					continue;
				}

				// is the item equipped on another character?
				if (item.equipped && _characterId !== item.characterId) {
					// QUEUE TO UNEQUIP ITEM FROM CHARACTER
					var alternateCharId = item.characterId;
					var availableSlotsInChar = _playerSlots[alternateCharId];
					
					// loop inventory items to find an item to equip in it's place
					for (var invItem in _inventory) {
						var inventoryItem = _inventory[invItem];
						// if item isn't on character skip it
						if (inventoryItem.characterId !== alternateCharId) {
							continue;
						}

						if (inventoryItem.equipped || inventoryItem.tierTypeName === "Exotic") {
							continue;
						}

						if (item.itemCategory === "Primary" || item.itemCategory === "Special" || item.itemCategory === "Heavy") {
							if (inventoryItem.itemCategory !== item.itemCategory) {
								continue;
							}

							equipItemOnCharacter(alternateCharId, inventoryItem.itemInstanceId, item, "UnEquip Item on Alternate Character");
						} else {
							if (inventoryItem.itemTypeName !== item.itemTypeName) {
								continue;
							}

							equipItemOnCharacter(alternateCharId, inventoryItem.itemInstanceId, item, "UnEquip Item on Alternate Character");
						}

						// break out of each inventory item loop
						break;
					}

					// QUEUE TO MOVE ITEM FROM ALTERNATE CHARACTER TO VAULT
					moveItemToVault(alternateCharId, item, "Move Item to Vault from Alternate Character");

					// QUEUE TO MOVE ITEM FROM VAULT TO CHARACTER
					moveItemFromVault(_characterId, item, "Move Item from Vault to Character");

					// QUEUE TO EQUIP ITEM ON CHARACTER
					equipItemOnCharacter(_characterId, item.itemInstanceId, item, "Equip Item on Character");

					continue;
				}

				// is the item not equipped on another character?
				if (!item.equipped && _characterId !== item.characterId) {
					// QUEUE TO MOVE ITEM FROM ALTERNATE CHARACTER TO VAULT
					moveItemToVault(item.characterId, item, "Move Item to Vault from Alternate Character");

					// QUEUE TO MOVE ITEM FROM VAULT TO CHARACTER
					//ensureOpenSlot(item);
					moveItemFromVault(_characterId, item, "Move Item from Vault to Character");

					// QUEUE TO EQUIP ITEM ON CHARACTER
					equipItemOnCharacter(_characterId, item.itemInstanceId, item, "Equip Item on Character");

					continue;
				}

				// is the item equipped on the character for the loadout?
				if (item.equipped && _characterId === item.characterId) {
					// splendid, nothing to do here. Move along...
					continue;
				}
			}

			return _queue;
		}

		// ensure there is an open slot for the type of item being transferred
		function ensureOpenSlot(item) {
			// check the item category
			switch (item.itemCategory) {
				case "Primary":
					// need to make a slot available for the character
					if (_playerSlots[_characterId].availablePrimarySlots <= 0) {
						// check inventory for a weapon of this type
						var inventoryItem = null;
						for (var itemObj in _inventory) {
							inventoryItem = _inventory[itemObj];
							if (inventoryItem.itemCategory === "Primary") {
								break;
							}
						}

						// TODO: make sure the found weapon isn't also part of the gearset

						// make sure there is vault space to store the item (we want at least 1 open slot at all times)
						if (_vaultSlots.availableWeaponSlots <= 1) {
							// not enough vault room, move to another character
							for (var alternateCharId in _playerSlots) {
								var charSlots = _playerSlots[alternateCharId];
								// if same as character we're transferring to, skip it
								if (alternateCharId === _characterId) {
									continue;
								}

								// if there is an open slot, move item to char
								if (charSlots.availablePrimarySlots > 0) {
									// move item to vault
									moveItemToVault(_characterId, inventoryItem, "Move Item to Vault from Character");

									// move item from vault to alternate character
									moveItemFromVault(alternateCharId, inventoryItem, "Move Item from Vault to Alternate Character");
									break;
								}
							}
						} else {
							// move item to vault
							moveItemToVault(_characterId, inventoryItem, "Move Item to Vault from Character");
						}

					}

					break;
			}
		}

		// queue moving an item to vault
		function moveItemToVault(characterId, item, description) {
			_queue.push(addQueueItem({
				url: '/?event=api.transferItemToVault',
				params: {
					characterId: characterId,
					itemId: item.itemInstanceId,
					itemReferenceHash: item.itemHash,
					stackSize: item.stackSize
				}
			}, description, item));
		}

		// queue moving an item from vault
		function moveItemFromVault(characterId, item, description) {
			_queue.push(addQueueItem({
				url: '/?event=api.transferItemFromVault',
				params: {
					characterId: characterId,
					itemId: item.itemInstanceId,
					itemReferenceHash: item.itemHash,
					stackSize: item.stackSize
				}
			}, description, item));
		}

		// queue equipping an item on a character
		function equipItemOnCharacter(characterId, equipItemId, item, description) {
			_queue.push(addQueueItem({
				url: '/?event=api.equipItem',
				params: {
					characterId: characterId,
					itemId: equipItemId
				}
			}, description, item));
		}

		// add a queue item
		function addQueueItem(endpoint, description, itemObj) {
			return {
				endpoint: endpoint,
				description: itemObj.itemName + ': ' + description,
				attempts: 0,
				complete: false,
				error: false,
				status: 'warning'
			};
		}

		return this;
	}
]);
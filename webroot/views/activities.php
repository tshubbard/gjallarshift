

<div class="container" ng-controller="ActivitiesCtrl">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>Activities</h3>
                https://www.bungie.net/Platform/Destiny/Stats/ActivityHistory/1/4611686018429697489/2305843009216266818/?lc=en&fmt=true&lcin=true&mode=0&count=20&page=0&definitions=true
                <?php $ainfo=$this->getValue("destinyAccountInfo"); ?>
                <?php foreach($ainfo as $key => $account) { ?>
                    <h4>Account Type: <?php echo ucfirst($key); ?></h4>
                    Account Name: <?php echo $account['playerName']; ?><br>
                    Account ID: <?php echo $account['playerID']; ?><br>
                    <?php foreach($account['characterIDs'] as $ckey => $char) {
                        $charDeets = $account['characterDetails'][$char]; ?>
                        Character <?php echo $ckey+1; ?>: <?php echo $char; ?> (<?php echo $charDeets['level'] . " " . $charDeets['race'] . " " . $charDeets['gender'] . " " . $charDeets['class']; ?>)<br>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

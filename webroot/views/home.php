<div class="container" ng-controller="AuthCtrl">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <div class="jumbotron">
            <div class="container">
              <h1>Hello, world!</h1>
              <p>We shift worlds, as well as minds.</p>
              <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
            </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4">
      <div class="panel panel-default">
        <div class="panel-body">
          <form ng-submit="login()">
            <div class="form-group">
              <div>
                <p>Log in through bungie to continue</p>
              </div>
            </div>         
            <div class="form-group pull-right">
              <button type="button" class="btn btn-success" ng-disabled="isProcessing"
                      ng-click="bungieAuth()">Log in</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

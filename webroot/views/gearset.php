<div id="gearsets" ng-controller="GearSetCtrl">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom:20px;">
                <div class="btn-group" data-toggle="buttons" ng-repeat="character in characters">
                    
                        <label class="btn btn-primary {{character.active? 'active' : ''}} charEmblem">
                            <span class="emblemBkgd">
                                <img src="https://www.bungie.net{{character.emblem.backgroundPath}}" />
                            </span>
                            <span class="emblemIcon">
                                <img src="https://www.bungie.net{{character.emblem.emblemPath}}" />
                            </span>
                            <span class="charClass">{{character.class}}</span>
                            <span class="charRaceGender">{{character.race}} {{character.gender}}</span>
                            <span class="charLevel">{{character.level}}</span>
                            <span class="charGrimoire">{{character.grimoire}}</span>

                            <input type="radio" name="activeCharacter" id="char-{{$index}}"
                                   autocomplete="off" value="{{character.id}}" {{$index == 0? 'checked' : ''}}>
                        </label>
                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="text-right gearset-toolbar">
                    <button type="button" class="btn btn-success" ng-click="showNewGearSet()">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add GearSet
                    </button>
                </div>
                <div class="list-group gearset-list">
                    <a class="list-group-item list-group-item-info" ng-if="addGearSet">
                        <div class="input-group">
                            <input type="text" class="form-control" ng-model="newGearSetName" placeholder="Enter GearSet Name..." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" ng-click="addNewGearSet(newGearSetName)">Add</button>
                            </span>
                        </div>
                    </a>
                    <a class="list-group-item {{gearset.active ? 'active' : ''}}" ng-click="configureGearSet(gearset.id)" ng-repeat="gearset in gearsets">{{gearset.name}}</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="section-title">
                    GearSet Settings
                    <div class="pull-right" ng-if="activeGearSet">
                        <button class="btn btn-success" ng-click="equipGearSet()">Use GearSet</button>
                    </div>
                </div>
                <hr />
                <div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div ng-if="activeGearSet" id="active-gearset">
                                <div class="row">
                                    <div class="col-xs-12" ng-repeat="panel in activeGearSet.panels">
                                        <div class="panel {{panel.active ? 'panel-primary' : 'panel-info'}}" id="{{panel.id}}">
                                            <div class="panel-heading">
                                                {{panel.name}}
                                            </div>
                                            <div class="panel-body">
                                                <div class="item" ng-click="selectItem('weapons', panel.id)">
                                                    <div class="item-thumb-container">
                                                        <img class="item-thumb" ng-src="{{items[itemIndex[panel.item]].icon}}" alt="{{items[itemIndex[panel.item]].itemName}}" />
                                                    </div>
                                                    <div class="item-info-container">
                                                        <div>{{items[itemIndex[panel.item]].itemName}}</div>
                                                        <div>{{items[itemIndex[panel.item]].primaryStat.name}} {{items[itemIndex[panel.item]].primaryStat.value}}</div>
                                                        <div>{{items[itemIndex[panel.item]].stats.Light.name}} {{items[itemIndex[panel.item]].stats.Light.value}}</div>
                                                        <div>{{items[itemIndex[panel.item]].damageType}}</div>
                                                    </div>
                                                </div>

                                                <div class="additional-items" ng-if="panel.additionalSlots">
                                                    <div class="additional-item" ng-repeat="additionalSlot in panel.additionalSlots" ng-click="selectItem('weapons', panel.id, additionalSlot.id)">
                                                        <img class="item-thumb" ng-src="{{items[itemIndex[additionalSlot.item]].icon}}" alt="{{items[itemIndex[additionalSlot.item]].itemName}}" />
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div ng-if="activeArmorGearSet" id="active-gearset">
                                <div class="row">
                                    <div class="col-xs-12" ng-repeat="panel in activeArmorGearSet.panels">
                                        <div class="panel {{panel.active ? 'panel-primary' : 'panel-info'}}" id="{{panel.id}}">
                                            <div class="panel-heading">
                                                {{panel.name}}
                                            </div>
                                            <div class="panel-body">
                                                <div class="item" ng-click="selectItem('armor', panel.id)">
                                                    <div class="item-thumb-container">
                                                        <img class="item-thumb" ng-src="{{items[itemIndex[panel.item]].icon}}" alt="{{items[itemIndex[panel.item]].itemName}}" />
                                                    </div>
                                                    <div class="item-info-container">
                                                        <div>{{items[itemIndex[panel.item]].itemName}}</div>
                                                        <div>{{items[itemIndex[panel.item]].primaryStat.name}} {{items[itemIndex[panel.item]].primaryStat.value}}</div>
                                                        <div>{{items[itemIndex[panel.item]].stats.Light.name}} {{items[itemIndex[panel.item]].stats.Light.value}}</div>
                                                        <div>{{items[itemIndex[panel.item]].damageType}}</div>
                                                    </div>
                                                </div>

                                                <div class="additional-items" ng-if="panel.additionalSlots">
                                                    <div class="additional-item" ng-repeat="additionalSlot in panel.additionalSlots" ng-click="selectItem('armor', panel.id, additionalSlot.id)">
                                                        <img class="item-thumb" ng-src="{{items[itemIndex[additionalSlot.item]].icon}}" alt="{{items[itemIndex[additionalSlot.item]].itemName}}" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-info" ng-if="!activeGearSet">Select a gearset to configure...</div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="text-right gearset-toolbar">
                    <button type="button" class="btn btn-success" ng-click="showNewGearSet()">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add GearSet
                    </button>
                </div>
                <div class="list-group gearset-list">
                    <a class="list-group-item list-group-item-info" ng-if="addGearSet">
                        <div class="input-group">
                            <input type="text" class="form-control" ng-model="newGearSetName" placeholder="Enter GearSet Name..." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" ng-click="addNewGearSet(newGearSetName)">Add</button>
                            </span>
                        </div>
                    </a>
                    <a class="list-group-item {{gearset.active ? 'active' : ''}}" ng-click="configureArmorGearSet(gearset.id)" ng-repeat="gearset in armorgearsets">{{gearset.name}}</a>
                </div>
            </div>
        </div>
    </div>

    <div id="item-list">
        <div class="item-container" id="item-{{$index}}" ng-hide="item.hide" ng-repeat="item in items">
            <div class="item" ng-click="moveItemToGearSet(item.itemInstanceId)">
                <div class="item-thumb-container">
                    <img class="item-thumb" ng-src="{{item.icon}}" alt="{{item.itemName}}" />
                </div>
                <div class="item-info-container">
                    <div>{{item.itemName}}</div>
                    <div>{{item.primaryStat.name}} {{item.primaryStat.value}}</div>
                    <div>{{item.stats.Light.name}} {{item.stats.Light.value}}</div>
                    <div>{{item.damageType}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="itemflow-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Item Flow</h4>
                </div>
                <div class="modal-body">
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-{{queue.status}}" ng-repeat="queue in itemFlowQueue">{{queue.description}}</li>
                    </ul>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade hide" id="bungie-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Log in to Bungie</h4>
                </div>
                <div class="modal-body">
                    <form ng-submit="bungieAuth()" ng-controller="AuthCtrl">
                        <div class="form-group">
                            <label for="username">Username (Email)</label>
                            <input type="email" id="username" name="username" placeholder="Username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>System</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="platformName" id="system-xbox" value="xbox" checked>
                                        Xbox
                                    </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="platformName" id="system-ps" value="psn">
                                        Playstation
                                    </label>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-success">Sign in</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

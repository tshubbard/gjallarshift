<div class="container" ng-controller="AuthCtrl">
    <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Account information:</h3>
                    <?php $ainfo=$this->getValue("destinyAccountInfo"); ?>
                    <?php foreach($ainfo as $key => $account) { ?>
                    <h4>Account Type: <?php echo ucfirst($key); ?></h4>
                    Account Name: <?php echo $account['playerName']; ?><br>
                    Account ID: <?php echo $account['playerID']; ?><br>
                    <?php foreach($account['characterIDs'] as $ckey => $char) { 
                        $charDeets = $account['characterDetails'][$char]; ?>
                    Character <?php echo $ckey+1; ?>: <?php echo $char; ?> (<?php echo $charDeets['level'] . " " . $charDeets['race'] . " " . $charDeets['gender'] . " " . $charDeets['class']; ?>)<br>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
    </div>
</div>

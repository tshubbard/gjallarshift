<?php
/*
 * Created on Dec 29, 2010
 *
 * PholdBox Config
 */

$SYSTEM = array();
$GLOBALS["SYSTEM"] = &$SYSTEM;

//-- Production values --

//Debug Output
$SYSTEM["debug"] = false;

//Datasources
$SYSTEM["dbBatchSize"] = 100;
$SYSTEM["dsn"] = array();
$SYSTEM["dsn"]["default"] = "gjallarshift";
// Add as many datasources as you like
// Pear DB syntax - phptype://username:password@protocol+hostspec/database
$SYSTEM["dsn"]["pholdbox"] = array();
$SYSTEM["dsn"]["pholdbox"]["connection_string"] = "mysql://CS120:isfun@localhost/pholdbox";
$SYSTEM["dsn"]["gjallarshift"]["connection_string"] = "mysql://root:root@localhost/gjallarshift";
$SYSTEM["clientId"] = 21457;
$SYSTEM["apiKey"] = "1e4ffb141da74d65b5f93cd7c44525d2";

//Default Layout/view
$SYSTEM["default_layout"] = "layout.main";
$SYSTEM["default_event"] = "main.home"; 

//per app settings go here
$SYSTEM["app"]["mysetting"] = "PholdBox Rocks!";

//-- Site Specific Configs --
//barlowdev
$SYSTEM["gshift.local.dev"]["dsn"]["gjallarshift"]["connection_string"] = "mysql://root:root@localhost/gjallarshift";

/************
 * Had to do this because when I ran the unit test, it kept erroring:
 *
 * 1) ActivityModelTest::testParseModel
 *      Undefined index: gjallarshift
 *
 *      /vagrant/webroot/system/PhORM.php:63
 *      /vagrant/webroot/tests/model/Activity.php:20
 *
 *
 * PLEASE LET ME KNOW IF THERE'S A BETTER WAY OR I DONT HAVE SOMETHING SET UP RIGHT!
 * <3 Travis Mortimer Quincy Hubbard VII jr.
 */
$SYSTEM["dsn"]["gjallarshift"]["connection_string"] = "mysql://root:root@localhost/gjallarshift";

//heacockdev
$SYSTEM["stark.theheacocks.com"]["dsn"]["gjallarshift"]["connection_string"] = "mysql://root:root@localhost/gjallarshift";

//production
$SYSTEM["www.destinygearsets.com"]["dsn"]["gjallarshift"]["connection_string"] = "mysql://gjallarshift:~!gjallarshift~!@localhost/gjallarshift";
$SYSTEM["www.destinygearsets.com"]["debug"] = false;
$SYSTEM["www.destinygearsets.com"]["clientId"] = 21459;
$SYSTEM["www.destinygearsets.com"]["apiKey"] = "7de72b023cee49b6a905def3ce8048a1";


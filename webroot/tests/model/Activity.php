<?php

require_once('tests/AppTestBase.php');
require_once('model/Activity.php');

class ActivityModelTest extends AppTestBase {
    protected $fixtures;

    protected function setUp() {
        parent::setUp();

        $defFile = file_get_contents('tests/fixtures/activitiesFixtures.json');
        $this->fixtures = json_decode($defFile);
    }

    public function testParseModel() {

        $activityDef = $this->fixtures->Response->data->activities[0];
        $defs = $this->fixtures->Response->definitions;
        $activity = new Activity();
        $activity->parse($activityDef, $defs);
        $this->assertEquals($activity->getValue('id'),                      '');
        $this->assertEquals($activity->getValue('accountId'),               '');
        $this->assertEquals($activity->getValue('instanceId'),              $activityDef->activityDetails->instanceId);
        $this->assertEquals($activity->getValue('characterId'),             '');
        $this->assertEquals($activity->getValue('mode'),                    $activityDef->activityDetails->mode);
        $this->assertEquals($activity->getValue('activityDate'),            $activityDef->period);
        $this->assertEquals($activity->getValue('referenceId'),             $activityDef->activityDetails->referenceId);

        $refId = $activityDef->activityDetails->referenceId;
        $definitionsActivityDef = $defs->activities->$refId;
        $this->assertEquals($activity->getValue('activityName'),            $definitionsActivityDef->activityName);

        $actDefVals = $activityDef->values;
        $this->assertEquals($activity->getValue('assists'),                 $actDefVals->assists->basic->value);
        $this->assertEquals($activity->getValue('score'),                   $actDefVals->score->basic->value);
        $this->assertEquals($activity->getValue('kills'),                   $actDefVals->kills->basic->value);
        $this->assertEquals($activity->getValue('averageScorePerKill'),     $actDefVals->averageScorePerKill->basic->value);
        $this->assertEquals($activity->getValue('deaths'),                  $actDefVals->deaths->basic->value);
        $this->assertEquals($activity->getValue('averageScorePerLife'),     $actDefVals->averageScorePerLife->basic->value);
        $this->assertEquals($activity->getValue('completed'),               $actDefVals->completed->basic->value);
        $this->assertEquals($activity->getValue('killsDeathsRatio'),        $actDefVals->killsDeathsRatio->basic->value);
        $this->assertEquals($activity->getValue('killsDeathsAssists'),      $actDefVals->killsDeathsAssists->basic->value);
        $this->assertEquals($activity->getValue('activityDurationSeconds'), $actDefVals->activityDurationSeconds->basic->value);
        $this->assertEquals($activity->getValue('standing'),                $actDefVals->standing->basic->value);
        $this->assertEquals($activity->getValue('team'),                    $actDefVals->team->basic->value);
        $this->assertEquals($activity->getValue('completionReason'),        $actDefVals->completionReason->basic->value);
        $this->assertEquals($activity->getValue('fireTeamId'),              $actDefVals->fireTeamId->basic->value);
        $this->assertEquals($activity->getValue('playerCount'),             $actDefVals->playerCount->basic->value);
        $this->assertEquals($activity->getValue('hasRewards'),              1);
        $this->assertEquals($activity->getValue('rewards'),                 json_encode($definitionsActivityDef->rewards));
        $this->assertEquals($activity->getValue('hasSkulls'),               1);
        $this->assertEquals($activity->getValue('skulls'),                  json_encode($definitionsActivityDef->skulls));






























    }
}

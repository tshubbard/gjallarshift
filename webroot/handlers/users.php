<?php
/*
 * This is an example class to show how you use events.  You can look for the documentation inside the
 * Event class itself to see what is available and how to use it.  I'll work on getting proper documentation
 * out there at some point -- honest :)
 * 
 * To use the functions below home(), you'll need to enable a DSN, create the table outlined in
 * MyObj, and make MyObj extend PhORM
 * 
 */
class Users extends system\Event
{
    protected $IOC = array("User", "lib.ServiceResponse");
    
    /**
     * register
     * 
     * registration endpoint
     */
    public function register()
    {
        $username = $this->getValue("username");
        $password = $this->getValue("password");
        
        $serviceResponse = $this->instance["lib.ServiceResponse"];
        
        $user = $this->instance["User"];
        $user->setEmail($username);
        $user->load();
        
        if($user->getId() != ""){
            $serviceResponse->success = false;
            array_push($serviceResponse->errors, "This email address is taken.");
        } else{
            $user->setPassword(hash("sha512", $password));
            $user->setLastLoginDate(strtotime("now"));
            $user->setActivePlatformID("");
            $user->save();
        }
        
        $this->setValue("json", json_encode($serviceResponse));
        $this->setView("json", false);
        $this->renderView();
    }
    
    public function login()
    {
        $username = $this->getValue("username");
        $password = $this->getValue("password");
        
        $serviceResponse = $this->instance["lib.ServiceResponse"];
        
        $user = $this->instance["User"];
        $user->setEmail($username);
        $user->load();
        
        if($user->getId() == ""){
            $serviceResponse->success = false;
            array_push($serviceResponse->errors, "Not Found");
        } else if($user->getPassword() != hash("sha512", $password)){
            $serviceResponse->success = false;
            array_push($serviceResponse->errors, "Invalid Username/Password combination");
        } else
        {
            $this->setSessionValue("username", $user->getEmail());
            $this->setSessionValue("activePlatformID", $user->getActivePlatformID());
            $this->setSessionValue("lastLoginDate", $user->getLastLoginDate());
            
            //get bungie session info
            $this->setSessionValue("bungled", $user->getBungled());
            $this->setSessionValue("bungleloc", $user->getBungleloc());
            $this->setSessionValue("bungleatk", $user->getBungleatk());
            $this->setSessionValue("cookie", $user->getCookies());
            $this->setSessionValue("destinyAccountInfo", unserialize(base64_decode($user->getDestinyAccountInfo())));
            
            if($user->getDestinyAccountInfo() != ""){
                $this->setSessionValue("displayName", $this->getSessionValue("destinyAccountInfo")['bungie']['playerName']);
                if (array_key_exists("xbox", $this->getSessionValue("destinyAccountInfo")))
                    $this->setSessionValue("activePlatformID", "xbox");
                if (array_key_exists("psn", $this->getSessionValue("destinyAccountInfo")))
                    $this->setSessionValue("activePlatformID", "psn");
            }
        }
        $this->setValue("json", json_encode($serviceResponse));
        $this->setView("json", false);
        $this->renderView();
    }
    
    public function logout()
    {
        $this->setSessionValue("username", "");
        $this->setSessionValue("bungled", "");
        $this->setSessionValue("bungleloc", "");
        $this->setSessionValue("bungleatk", "");
        $this->setSessionValue("cookie", "");
        $this->setSessionValue("destinyAccountInfo", "");
        $this->setSessionValue("displayName", "");
        $this->setSessionValue("activePlatformID", ""); 
        
        $this->runEvent("main.home");
    }
}

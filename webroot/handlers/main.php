<?php
/*
 * This is an example class to show how you use events.  You can look for the documentation inside the
 * Event class itself to see what is available and how to use it.  I'll work on getting proper documentation
 * out there at some point -- honest :)
 * 
 * To use the functions below home(), you'll need to enable a DSN, create the table outlined in
 * MyObj, and make MyObj extend PhORM
 * 
 */
class Main extends system\Event
{
    protected $IOC = array("lib.BungieAPI", "lib.ServiceResponse", "User");
    
    /**
     * preEvent
     * 
     * Fires before any other event in this object
     */
    public function preEvent()
    {
        $event = $this->getValue("event");
        if($event != "main.home" && $event != "main.auth" && $event != "main.register" && $event != "main.getClientId"){
            if($this->getSessionValue("displayName") == ""){
                $this->runEvent("main.home");
            }
        }
    }
    
    /**
     * home
     * 
     * main home view
     */
    public function home()
    {
        $this->setView("home");
        $this->renderView();
    }

    public function getClientId()
    {
        $result = new stdClass();
        $result->clientId = $this->SYSTEM["clientId"];
        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }
    
    
    public function auth()
    {
       $code = $this->getValue("code");
       $result = $this->instance["lib.BungieAPI"]->getToken($code);
       $user = $this->instance["User"];

        if($result->success && !property_exists($result->data[0], "error"))
        {
            $this->setSessionValue("access_token", $result->data[0]->access_token);
            $this->setSessionValue("membership_id", $result->data[0]->membership_id);
            $response = $this->instance["lib.BungieAPI"]->getAccountInfo();
            
            $this->setSessionValue("destinyAccountInfo", $response);
            $this->setSessionValue("displayName", $response['bungie']['playerName']);
            
            if (array_key_exists("xbox", $this->getSessionValue("destinyAccountInfo")))
                $this->setSessionValue("activePlatformID", "xbox");
            if (array_key_exists("psn", $this->getSessionValue("destinyAccountInfo")))
                $this->setSessionValue("activePlatformID", "psn");
                
            $user->setMembershipId($this->getSessionValue("membership_id"));
            $user->load();
            $user->setLastLoginDate(strtotime("now"));
            $user->setActivePlatformID($this->getSessionValue("activePlatformID"));
            $user->setDestinyAccountInfo(base64_encode(serialize($response)));
            $user->save();
        
            header("Location: /main/gearsets");
        }
        else {
            echo "error";
        }
       
    }

    /**
     * gearsets
     * 
     * goes to the gearset page
     */
    public function gearsets()
    {
        $this->setView("gearset");
        $this->renderView();
    }
    
    /**
     * gets character data for front end
     */
    public function characters()
    {
        $serviceResponse = $this->instance["lib.ServiceResponse"];
        $destinyAccountInfo = $this->getSessionValue("destinyAccountInfo");
        $platform = $this->getValue("platform");
        
        if($platform != ""){
            $this->setSessionValue("activePlatformID", $platform);
        } 
        
        $platform = $this->getSessionValue("activePlatformID");
        
        if(($destinyAccountInfo != "")  && ($platform != "")){
            $platformData = $destinyAccountInfo[$platform];
            $serviceResponse->data = $platformData["characterDetails"];
        } else {
            $serviceResponse->success = false;
            array_push($serviceResponse->errors, "No character data.");
        }
        
        $this->setValue("json", json_encode($serviceResponse));
        $this->setView("json", false);
        $this->renderView();
    }
    
    /**
     * returns active accounts/platforms
     */
    public function accounts()
    {
        $serviceResponse = $this->instance["lib.ServiceResponse"];
        $destinyAccountInfo = $this->getSessionValue("destinyAccountInfo");
        $activePlatform = $this->getSessionValue("activePlatformID");
        $accounts = array();
        if($destinyAccountInfo != ""){
            foreach($destinyAccountInfo as $key => $value){
                $account = new \stdClass();
                $account->type = $key;
                $account->active = ($activePlatform == $key) ? "active" : "";
                array_push($accounts, $account);
            }
        }
        
        $serviceResponse->data = $accounts;
        $this->setValue("json", json_encode($serviceResponse));
        $this->setView("json", false);
        $this->renderView();
    }
    
    /**
     * account
     * 
     * manages account
     */
    public function account()
    {
        $this->setValue("destinyAccountInfo",$this->getSessionValue("destinyAccountInfo"));

        $this->setView("account");
        $this->renderView();
    }

    public function activities() {
        $this->setValue("destinyAccountInfo",$this->getSessionValue("destinyAccountInfo"));

        $this->setView("activities");
        $this->renderView();
    }
}

<?php
/*
 * This is an example class to show how you use events.  You can look for the documentation inside the
 * Event class itself to see what is available and how to use it.  I'll work on getting proper documentation
 * out there at some point -- honest :)
 * 
 * To use the functions below home(), you'll need to enable a DSN, create the table outlined in
 * MyObj, and make MyObj extend PhORM
 * 
 */
class Api extends system\Event
{
    protected $IOC = array("lib.BungieAPI", "lib.ServiceResponse");

    public function preEvent()
    {
        if($this->getSessionValue("membership_id") == ""){
            $this->runEvent("main.home");
        }
    }
    
    /**
     * Transfers Item to or from the vault
     * 
     * Post vars:
     *      characterId
     *      itemId
     *      itemReferenceHash
     *      stackSize (typically 1)
     *      transferToVault (boolean)
     */
    public function transferItem()
    {
        $membershipType = $this->getSessionValue("destinyAccountInfo")[$this->getSessionValue("activePlatformID")]['platformID'];
        $characterId = $this->getValue("characterId");
        $itemId = $this->getValue("itemId");
        $itemReferenceHash = $this->getValue("itemReferenceHash");
        $stackSize = $this->getValue("stackSize");
        $transferToVault = $this->getValue("transferToVault");
        
        $result = $this->instance["lib.BungieAPI"]->transferItem($membershipType, $characterId, $itemId, $itemReferenceHash, $stackSize, $transferToVault);
        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }
    
    /**
     * equips item
     * 
     * Post vars:
     *      characterId
     *      itemId (reference hash)
     */
    public function equipItem()
    {
        $membershipType = $this->getSessionValue("destinyAccountInfo")[$this->getSessionValue("activePlatformID")]['platformID'];
        $characterId = $this->getValue("characterId");
        $itemId = $this->getValue("itemId");
        
        $result = $this->instance["lib.BungieAPI"]->equipItem($membershipType, $characterId, $itemId);
        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }
    
    /**
     * Transfers Item to the vault
     * 
     * Post vars:
     *      characterId
     *      itemId
     *      itemReferenceHash
     *      stackSize (typically 1)
     */
    public function transferItemToVault()
    {
        $membershipType = $this->getSessionValue("destinyAccountInfo")[$this->getSessionValue("activePlatformID")]['platformID'];
        $characterId = $this->getValue("characterId");
        $itemId = $this->getValue("itemId");
        $itemReferenceHash = $this->getValue("itemReferenceHash");
        $stackSize = $this->getValue("stackSize");
        $transferToVault = true;
        
        $result = $this->instance["lib.BungieAPI"]->transferItem($membershipType, $characterId, $itemId, $itemReferenceHash, $stackSize, $transferToVault);
        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }
    
    /**
     * Transfers Item from the vault
     * 
     * Post vars:
     *      characterId
     *      itemId
     *      itemReferenceHash
     *      stackSize (typically 1)
     */
    public function transferItemFromVault()
    {
        $membershipType = $this->getSessionValue("destinyAccountInfo")[$this->getSessionValue("activePlatformID")]['platformID'];
        $characterId = $this->getValue("characterId");
        $itemId = $this->getValue("itemId");
        $itemReferenceHash = $this->getValue("itemReferenceHash");
        $stackSize = $this->getValue("stackSize");
        $transferToVault = false;
        
        $result = $this->instance["lib.BungieAPI"]->transferItem($membershipType, $characterId, $itemId, $itemReferenceHash, $stackSize, $transferToVault);
        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }

    public function getActivities() {
        $destinyAccountInfo = $this->getSessionValue("destinyAccountInfo");
        $platformId = $this->getSessionValue("activePlatformID");
        $membershipType = $destinyAccountInfo[$platformId]['platformID'];
        $accountId = $destinyAccountInfo[$platformId]['playerID'];
        $characterId = $this->getValue("characterId");
        $mode = $this->getValue("mode");
        $count = $this->getValue("count");

        $result = $this->instance["lib.BungieAPI"]->getActivities($membershipType, $accountId, $characterId, $mode, $count);

        $activities = $this->parseActivities($result->Response->data->activities, $result->Response->definitions);

        $result->Response->data->activities = array();
        foreach($activities as $act) {
            $result->Response->data->activities[] = $act->toStdClass();
        }

        $this->setValue("json", json_encode($result));
        $this->setView("json", false);
        $this->renderView();
    }

    protected function parseActivities($activities, $defs) {
        require_once('model/Activity.php');

        $parsedActivities = array();
        foreach($activities as $act) {
            $activity = new Activity();
            $activity->parse($act, $defs);
            $parsedActivities[] = $activity;
        }

        return $parsedActivities;
    }

    /**
     * Gets player and vault inventory, returns as json.
     */
    public function getInventory()
    {
        $destinyAccountInfo = $this->getSessionValue("destinyAccountInfo");
        $activePlatformID = "";
        $activePlayerID = "";
        $activeCharacterIDs = array();
        $serviceResponse = $this->instance["lib.ServiceResponse"];
        
        $returnObj = new \stdClass();
        $returnObj->items = array();
        $returnObj->player = array();
        $returnObj->vault = new \stdClass();
        $returnObj->vault->availableArmorSlots = 20;
        $returnObj->vault->availableWeaponSlots = 20;
        $returnObj->vault->availableItemSlots = 20;
        $returnObj->itemIndex = new \stdClass();
        $items = &$returnObj->items;
        
        if($destinyAccountInfo != ""){
            $activePlatformID = $destinyAccountInfo[$this->getSessionValue("activePlatformID")]['platformID'];
            $activePlayerID = $destinyAccountInfo[$this->getSessionValue("activePlatformID")]['playerID'];
            $activeCharacterIDs = $destinyAccountInfo[$this->getSessionValue("activePlatformID")]['characterIDs'];
        
            $this->getVaultInventory($returnObj, $activePlatformID);
            
            foreach($activeCharacterIDs as $theChar) {
                $returnObj->player[$theChar] = new \stdClass();
                $returnObj->player[$theChar]->availableHelmetSlots = 10;
                $returnObj->player[$theChar]->availableChestSlots = 10;
                $returnObj->player[$theChar]->availableGauntletSlots = 10;
                $returnObj->player[$theChar]->availableLegSlots = 10;
                $returnObj->player[$theChar]->availablePrimarySlots = 10;
                $returnObj->player[$theChar]->availableSecondarySlots = 10;
                $returnObj->player[$theChar]->availableHeavySlots = 10;
                
                $this->getCharacterInventory($returnObj, $activePlatformID, $activePlayerID, $theChar);
            }
                
            
            //sort the final array by primary stat
            usort($items, function($item1, $item2){
                $item1Stat = 0;
                $item2Stat = 0;
                
                if(property_exists($item1->primaryStat, "value")){
                    $item1Stat = $item1->primaryStat->value;
                }
                
                if(property_exists($item2->primaryStat, "value")){
                    $item2Stat = $item2->primaryStat->value;
                }
                
                return $item2Stat - $item1Stat;
                
            });
            
            //create item index
            $count = 0;
            foreach($items as $item){
                $instanceId = $item->itemInstanceId;
                $returnObj->itemIndex->$instanceId = $count;
                $count++;
            }
        } else{
            $serviceResponse->success = false;
        }
        array_push($serviceResponse->data, $returnObj);
        $this->setValue("json", json_encode($serviceResponse));
        $this->setView("json", false);
        $this->renderView();
        
    }
    
    /**
     * protected function to get vault inventory
     */
    protected function getVaultInventory(&$returnObj, $platformID)
    {
        $result = $this->instance["lib.BungieAPI"]->getVaultInventory($platformID);
        if(!is_null($result)){
            $this->processInventory($returnObj, $result, $result->Response->data->buckets, "", true);
        }
        
    }
    
    /**
     * protected function to get character inventory
     */
    protected function getCharacterInventory(&$returnObj, $platformID, $playerID, $characterID)
    {
        $result = $this->instance["lib.BungieAPI"]->getCharacterInventory($platformID, $playerID, $characterID);
        if(!is_null($result)){
            $this->processInventory($returnObj, $result, $result->Response->data->buckets->Equippable, $characterID);
            $this->processInventory($returnObj, $result, $result->Response->data->buckets->Item, $characterID);
        }
    }
    
    /**
     * protected function to process inventory
     */
    protected function processInventory(&$returnObj, $result, $buckets, $characterId, $inVault = false)
    {
        $definitions = $result->Response->definitions;
        $items = &$returnObj->items;
        
        foreach($buckets as $bucket){
            //follow this structure for more items
            foreach($bucket->items as $item){
                $obj = new \stdClass();
                $obj->itemCategory = "";
                //decrement counts
                $bucketHash = $bucket->bucketHash;
                switch($definitions->buckets->$bucketHash->bucketName){
                    case "Armor":
                        $returnObj->vault->availableArmorSlots--;
                        break;
                    case "General":
                        $returnObj->vault->availableItemSlots--;
                        break;
                    case "Weapons":
                        $returnObj->vault->availableWeaponSlots--;
                        break;
                    case "Helmet":
                        $returnObj->player[$characterId]->availableHelmetSlots--;
                        break;
                    case "Chest Armor":
                        $returnObj->player[$characterId]->availableChestSlots--;
                        break;
                    case "Gauntlets":
                        $returnObj->player[$characterId]->availableGauntletSlots--;
                        break;
                    case "Leg Armor":
                        $returnObj->player[$characterId]->availableLegSlots--;
                        break;
                    case "Primary Weapons":
                        $returnObj->player[$characterId]->availablePrimarySlots--;
                        $obj->itemCategory = "Primary";
                        break;
                    case "Special Weapons":
                        $returnObj->player[$characterId]->availableSecondarySlots--;
                        $obj->itemCategory = "Special";
                        break;
                    case "Heavy Weapons":
                        $returnObj->player[$characterId]->availableHeavySlots--;
                        $obj->itemCategory = "Heavy";
                        break;
                }
                
                //gather items
                
                $obj->equipped = $item->isEquipped;
                $obj->inVault = $inVault;
                $obj->characterId = $characterId;
                $obj->itemHash = $item->itemHash;
                $hash = $obj->itemHash;
                $obj->itemInstanceId = $item->itemInstanceId;
                $obj->stackSize = $item->stackSize;
                $obj->itemName = $definitions->items->$hash->itemName;
                $obj->itemDescription = "";
                if(property_exists($definitions->items->$hash, "itemDescription")){
                    $obj->itemDescription = $definitions->items->$hash->itemDescription;
                }
                
                $obj->icon = "https://www.bungie.net" . $definitions->items->$hash->icon;
                $obj->tierTypeName = $definitions->items->$hash->tierTypeName;
                $obj->itemTypeName = $definitions->items->$hash->itemTypeName;
                $obj->classType = $definitions->items->$hash->classType;
                
                $baseStat = new \stdClass();
                if(property_exists($item, "primaryStat"))
                {
                    $statHash = $item->primaryStat->statHash;
                    $baseStat->name = $definitions->stats->$statHash->statName;
                    $baseStat->value = $item->primaryStat->value;
                    $baseStat->maximumValue = $item->primaryStat->maximumValue;
                }
                $obj->primaryStat = $baseStat;
                
                $obj->stats = array();
                if(property_exists($item, "stats"))
                {
                    foreach($item->stats as $stat)
                    {
                        $statHash = $stat->statHash;
                        $baseStat = new \stdClass();
                        $baseStat->name = $definitions->stats->$statHash->statName;
                        $baseStat->value = $stat->value;
                        $obj->stats[$baseStat->name] = $baseStat;
                    }
                }
                
                //damagetype
                switch($item->damageType){
                    case 2:
                        $obj->damageType = "Arc Damage";
                        break;
                    case 3:
                        $obj->damageType = "Solar Damage";
                        break;
                    case 4:
                        $obj->damageType = "Void Damage";
                        break;
                    default:
                        $obj->damageType = "";
                }
                
                //if we didn't find the item's category above, so we need to do it manually
                if($obj->itemCategory == ""){
                    switch($obj->itemTypeName){
                        case "Pulse Rifle":
                        case "Scout Rifle":
                        case "Auto Rifle":
                        case "Hand Canon":
                            $obj->itemCategory = "Primary";
                            break;
                        case "Sniper Rifle":
                        case "Fusion Rifle":
                        case "Shotgun":
                            $obj->itemCategory = "Special";
                            break;
                        case "Rocket Launcher":
                        case "Machine Gun":
                            $obj->itemCategory = "Heavy";
                            break;
                    }
                }
                array_push($items, $obj);
            }
        }
    }
}

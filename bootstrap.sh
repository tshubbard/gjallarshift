debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

#install necessary software
sudo apt-get update
sudo apt-get install -y apache2 php5 mysql-server php5-curl php5-ldap php5-mysql git php-pear phpunit 
sudo pear install MDB2 MDB2_Driver_mysql
sudo a2enmod rewrite

# update MySQL to not escape slashes
cat > /etc/mysql/conf.d/mysqld_no_backslash_escape.cnf<<EOD
[mysqld]
sql_mode='NO_BACKSLASH_ESCAPE'
EOD
#remove bind address
sudo sed -i '/bind-address/d' /etc/mysql/my.cnf
# restart MySQL
service mysql restart
# And we're done!

#update php error_log location
sudo echo "error_log = /var/www/html/php_errors.log" >> /etc/php5/apache2/php.ini

#set up app database
mysql --user=root --password=root < /vagrant/schema.sql

#set up Bungie database
mysql --user=root --password=root < /vagrant/bungieSchema.sql

#set up permissions
mysql --user=root --password=root -e "grant all PRIVILEGES on *.* to 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;"

#set up apache config
sudo echo '<VirtualHost *:80>' > /etc/apache2/sites-available/gshift.local.dev.conf
#sudo echo '       ServerName gshift.local.dev' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       ServerAdmin webmaster@localhost' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       DocumentRoot /vagrant/webroot' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       <Directory "/vagrant/webroot">' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '               Options All Indexes' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '               Order Allow,Deny' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '               Allow From All' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '               Require all granted' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '               AllowOverride All' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       </Directory>' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       ErrorLog ${APACHE_LOG_DIR}/gshift.error.log' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '       CustomLog ${APACHE_LOG_DIR}/gshift.access.log combined' >> /etc/apache2/sites-available/gshift.local.dev.conf
sudo echo '</VirtualHost>' >> /etc/apache2/sites-available/gshift.local.dev.conf

sudo ln -s /etc/apache2/sites-available/gshift.local.dev.conf /etc/apache2/sites-enabled/gshift.local.dev.conf
sudo rm /etc/apache2/sites-enabled/000-default.conf
sudo chown www-data:www-data /var/www/html
sudo service apache2 restart
